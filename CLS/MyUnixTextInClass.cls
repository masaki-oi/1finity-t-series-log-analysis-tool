VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "MyUnixTextInClass"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'-------------------------------------------------------------
' All Rights Reserved, Copyright (C) FUJITSU LIMITED 2015-2017
'
'   テキストファイル読み込み処理    (改行文字がUnix形式)
'           ※CRは無視する
'
'   更新日: 2017.03.10
'-------------------------------------------------------------
Option Explicit

Private myTextStream    As Object
Private mySourceName As String

Private myLineCount    As Long ' 行数カウンタ
Private myLineText As String   ' レコード内容
Private myFile_name As String
Private myBuffMax   As Long
Private myWordLen   As Long
Private mySbuff As String ' 読み込んだテキスト
Private myVerbose_flag  As Boolean

Public Sub Class_Initialize()
End Sub

Public Sub Class_Terminate()
    On Error Resume Next
    myTextStream.Close
    Set myTextStream = Nothing
    
    Application.StatusBar = False
    Err.Clear
End Sub

Public Property Get LineText() As String
    LineText = myLineText
End Property

Public Property Get LineNo() As Long
    LineNo = myLineCount
End Property

Public Property Get SourceName() As String
    SourceName = mySourceName
End Property

'--------------------------------------------------------------
' ファイルopen
'
'   strFileName: OPENするファイル名(フルパス)
'--------------------------------------------------------------
Public Function Initialize( _
        ByVal strFileName As String, _
        Optional ByVal Verbose As Boolean = True) As Boolean
        
    Dim fso As Object
    
    Initialize = False
    
    On Error GoTo Error_Exit
    
    ' 指定ファイルをOPEN
    Set fso = CreateObject("Scripting.FileSystemObject")
    Set myTextStream = fso.OpenTextFile(strFileName)
    
    mySbuff = ""
    myFile_name = strFileName
    myLineCount = 0
    myLineText = ""
    myVerbose_flag = Verbose
    
    mySourceName = strFileName
    
    Initialize = True
    
    Set fso = Nothing
    Exit Function
    
Error_Exit:
    MsgBox "'" & strFileName & "'" & vbCrLf _
        & " が open できません" & vbCrLf & vbCrLf _
        & Err.Description, vbCritical
    Set fso = Nothing
    
End Function

Public Sub Activate(Optional line_no As Variant = 1)
    '未実装
End Sub

Public Function seek_line(ByVal line_no As Long) As Boolean
    
    seek_line = False
    
    If line_no < myLineCount Then  '行き過ぎている場合は
        myTextStream.Close
        Set myTextStream = Nothing
        Call Initialize(myFile_name, myVerbose_flag) '最初から読み直す
    End If
    
    Do While myLineCount < line_no
        If record_read() = False Then
            Exit Function
        End If
    Loop
    
    seek_line = True
    
End Function

Public Function record_read() As Boolean
    Dim ss  As String
    Dim ii  As Long
    
    If mySbuff = "" Then
        myLineText = ""
        If myTextStream.AtEndOfstream Then
            record_read = False
            Exit Function
        End If
    End If
    
    ' レコード件数カウンタの加算
    If myVerbose_flag Then
        Application.StatusBar = mySourceName & " 読み込み中...(" & myLineCount + 1 & "レコード目)"
    End If
    
    ' 改行まで読み込む
    ss = mySbuff
    Do Until InStr(ss, vbLf) > 0
        If myTextStream.AtEndOfstream Then
            Exit Do
        End If
        ss = myTextStream.Read(500)
        mySbuff = mySbuff & ss
    Loop

    ii = InStr(mySbuff, vbLf)
    If ii > 0 Then
        ss = Left(mySbuff, ii - 1)
        mySbuff = Mid(mySbuff, ii + 1)
    Else    'ファイルの最終行
        ss = mySbuff
        mySbuff = ""
    End If
    
    myLineCount = myLineCount + 1
    myLineText = Replace(ss, vbCr, "")
    
    record_read = True

End Function
