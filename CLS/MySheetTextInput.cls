VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "MySheetTextInput"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'-------------------------------------------------------------
' All Rights Reserved, Copyright (C) FUJITSU LIMITED 2015
'
'   テキストファイル読み込み処理    (改行文字がUnix形式)
'           ※CRは無視する
'
'   更新日: 2015.08.28
'-------------------------------------------------------------
Option Explicit

Private myTextArea As Range '入力範囲
Private myTextRow As Range  '処理中の行

Private myTextRowCount As Variant '入力範囲の総行数
Private myTextColCount As Variant '入力範囲の総列数

Private myTextTopRow As Variant '入力範囲の上端の行番号
Private myTextEndRow As Variant '入力範囲の下端の行番号

Private myTextMinCol As Variant '入力範囲の左端の列番号
Private myTextMaxCol As Variant '入力範囲の右端の列行番号

Private myLineCount    As Variant ' 行数カウンタ
Private myLineText As String   ' レコード内容
Private mySourceName As String

Private myVerbose_flag  As Boolean

Public Sub Class_Initialize()
End Sub

Public Sub Class_Terminate()
    Set myTextRow = Nothing
    Set myTextArea = Nothing
    
    Application.StatusBar = False
End Sub

Public Property Get LineText() As String
    LineText = myLineText
End Property

Public Property Get LineNo() As Variant
    LineNo = myLineCount
End Property

Public Property Get SourceName() As String
    SourceName = mySourceName
End Property

Public Function Initialize( _
        ByVal varInput As Range, _
        Optional ByVal Verbose As Boolean = True) As Boolean
            
    Dim ws As Worksheet
    Dim r1 As Range
    Dim strName As String
    
    Initialize = False
    
    ' 有効範囲を取得
    Select Case TypeName(varInput)
    Case "Worksheet"
        Set ws = varInput
        Set r1 = ws.UsedRange
        strName = ""
        
    Case "Range"
        Set r1 = varInput
        Set ws = r1.Worksheet
        strName = "!" & r1.Address(False, False)
        
    Case Else
        Call MsgBox("Bug!!", vbCritical)
        Exit Function
    End Select
    
    strName = ws.Name & strName
    If Not ws.Parent Is ThisWorkbook Then
        strName = ws.Parent.Name & strName
    End If

    Set myTextArea = r1
    Set myTextRow = Nothing
    
    With myTextArea
        myTextRowCount = .Rows.Count
        myTextColCount = .Columns.Count
        myTextTopRow = .Row
        myTextEndRow = .Row + .Rows.Count - 1
        myTextMinCol = .Column
        myTextMaxCol = .Column + .Columns.Count - 1
    End With
    
    mySourceName = strName
    myLineCount = 0
    myLineText = ""
    myVerbose_flag = Verbose
    
    Set ws = Nothing
    Set r1 = Nothing
    
    Initialize = True
End Function

Public Sub Activate(Optional intLineNo As Variant = 1)
    myTextArea.Worksheet.Parent.Activate
    myTextArea.Worksheet.Activate
    myTextArea.Rows(intLineNo).Cells(1).Activate
End Sub

Public Function seek_line(ByVal intLineNo As Variant) As Boolean
    
    Select Case intLineNo
    Case 0
        Set myTextRow = Nothing
        myLineCount = 0
        myLineText = ""
        seek_line = True
        
    Case Is < 0, Is > myTextRowCount
        seek_line = False
        
    Case myLineCount
        seek_line = True
        
    Case Else
        myLineCount = intLineNo - 1
        If myLineCount = 0 Then
            Set myTextRow = Nothing
        Else
            Set myTextRow = myTextArea.Rows(myLineCount)
        End If
        seek_line = record_read()
    End Select
    
End Function

Public Function record_read() As Boolean
    
    myLineText = ""
    If myTextRow Is Nothing Then
        Set myTextRow = myTextArea.Rows(1)
    ElseIf myTextRow.Row = myTextEndRow Then
        record_read = False
        Exit Function
    Else
        Set myTextRow = myTextRow.Offset(1)
    End If
    
    ' レコード件数カウンタの加算
    If myVerbose_flag Then
        Application.StatusBar = mySourceName & " 読み込み中...(" & myLineCount + 1 & "レコード目)"
    End If
    
    ' 改行まで読み込む
    Dim aBuf() As String
    Dim c1 As Range
    Dim i As Variant
    
    ReDim aBuf(myTextColCount)
    i = 0
    For Each c1 In myTextRow.Columns
        If c1.HasFormula Then
            aBuf(i) = c1.Formula
        Else
            aBuf(i) = c1.Value
        End If
        i = i + 1
    Next c1
    
    myLineCount = myLineCount + 1
    myLineText = RTrim(Join(aBuf, ""))
    
    Erase aBuf
    
    record_read = True
End Function
