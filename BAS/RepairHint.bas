Attribute VB_Name = "RepairHint"
'-------------------------------------------------------------
' All Rights Reserved, Copyright (C) FUJITSU LIMITED 2017
'
'   部品対応表検索
'
'   更新日: 2017年 8月31日
'
'-------------------------------------------------------------
Option Explicit

Public Function 検索_Execute( _
        Optional target_rng As Range = Nothing) As Variant
        
    Const job_name = "部品対応表検索"
    
    Dim ret_code   As Variant
    Dim wsOut  As Worksheet
    Dim in_base As Range
    Dim out_base    As Range
    
    Dim last_row1   As Variant
    Dim c1  As Range
    
    Dim No_str    As String
    Dim ID_str  As String
    Dim data1_str   As String
    Dim data2_str   As String
    Dim name_str    As String
    Dim out_row As Variant
    Dim in_type As Long
    
    Dim num1    As Long
    Dim num2    As Long
    Dim outMode As Long
    Dim top_row As Variant
    
    Dim unit_figno  As String  'UNIT図番
    Dim varRefTable As Variant
    Dim intRecCount As Long
    Dim varAddr As Variant
    
    Call area_clear(2)
    
    'UNIT図版取得
    unit_figno = toolPara_get("UnitFigNo", 省略可:=False)
    If unit_figno = "" Then
        ret_code = -1
        GoTo Exit_Proc
    End If
    
    Set wsOut = GetWorkSheetByName(control_shname, ThisWorkbook)
    If verbose_flag Then Call MySheetActivate(wsOut)
    
    Set in_base = wsOut.Columns(d1_data_base_col).Rows(data_base_line)
    Set out_base = wsOut.Columns(d2_data_base_col)
    
    If target_rng Is Nothing Then
        Set c1 = in_base
        last_row1 = get_lastDataCell(wsOut).Row
    ElseIf target_rng.Row <= in_base.Row Then
        MsgBox "選択された行には有効なデータがありません", vbCritical
        ret_code = 0
        GoTo Exit_Proc
    Else
        Set c1 = in_base.Offset(target_rng.Row - in_base.Row - 1)
        last_row1 = target_rng.Row + target_rng.Rows.Count - 1
    End If
    
    outMode = detectMode_get()
    
    Call job_begin(job_name)
    
    varRefTable = Empty
    intRecCount = 0
    num1 = 0
    num2 = 0
    out_row = data_base_line
    Do While c1.Row < last_row1
        Set c1 = c1.Offset(1, 0)
        No_str = c1.Offset(0, d1_No_ofs)
        If No_str = "" Then
            Exit Do
        End If
        
        Application.StatusBar = "No. " & No_str & " 検索中..."
        intRecCount = intRecCount + 1
        If intRecCount >= 100 Then
            DoEvents
            intRecCount = 0
        End If
        
        ID_str = c1.Offset(0, d1_ID_ofs)
        data1_str = Trim(c1.Offset(0, d1_data1_ofs))
        data2_str = Trim(c1.Offset(0, d1_data2_ofs))
        name_str = Trim(c1.Offset(0, d1_Info_ofs))
        
        in_type = 0
        If (data1_str = "") And (data2_str = "") And (name_str <> "") Then
            in_type = 1
        Else
            If data1_str <> "" Then
                ' T700/QSFPの場合、pasre_addrはCallせずに生値を使用
                varAddr = Split(data1_str, ":")
                If (UBound(varAddr) > 1) Then
                  ' NOP
                Else
                  data1_str = pasre_addr(data1_str, 8)
                End If
            End If
        
            If data2_str <> "" Then
                data2_str = pasre_addr(data2_str, 8)
            End If
            
            If data1_str = "" Then
            ElseIf data2_str = "" Then
                Call set_error_mark(c1.Offset(0, d1_data2_ofs))
            Else
                in_type = 2
            End If
        End If
        
        If in_type > 0 Then
            num1 = num1 + 1
            
            If IsEmpty(varRefTable) Then
                varRefTable = RepairPartTableGet(unit_figno)
                If IsEmpty(varRefTable) Then
                    ret_code = -1
                    GoTo Exit_Proc
                End If
            End If
            
            top_row = out_row
            If in_type = 1 Then
                If Not detect_RepPartByLogname(varRefTable, name_str, out_base, out_row) Then
                    in_type = 0
                End If
            ElseIf Not detect_RepairPart(varRefTable, data1_str, data2_str, out_base, out_row) Then
                in_type = 0
            End If
            
            If in_type > 0 Then
                With c1.Resize(1, d1_data_max_ofs + 1)
                    .Interior.Color = vbCyan
                End With
            End If
            Do While top_row < out_row
                num2 = num2 + 1
                top_row = top_row + 1
                With out_base.Rows(top_row)
                    Call set_result(.Offset(0, d2_No_ofs), No_str)
                    Call set_result(.Offset(0, d2_ID_ofs), ID_str)
                End With
            Loop
        End If
Continue:
    Loop
    
    ret_code = num2 '検出数を返す。
    If num1 = 0 Then '検索対象が 0 件の場合
        MsgBox "「data1」「data2」欄に有効なデータがありません"
    End If
       
Exit_Proc:
    Dim strMsg  As String
    Select Case ret_code
    Case Is > 0
        Call MyRangeSelect(wsOut.Cells(data_base_line, d2_data_base_col))
        strMsg = "完了"
    Case 0
        strMsg = "終了(有効データ無し)"
        
    Case Is < 0
        strMsg = "中止(設定エラー)"
    End Select
    
    Call job_end(job_name, strMsg)
    
    Set in_base = Nothing
    Set out_base = Nothing
    If Not IsEmpty(varRefTable) Then
        Erase varRefTable
    End If
    Set c1 = Nothing
    Set wsOut = Nothing
    
    検索_Execute = ret_code
    
End Function

'--------------------------------------------------------------
' 16進アドレス正規化
'--------------------------------------------------------------
Public Function pasre_addr( _
        ByVal in_str As String, _
        Optional ByVal out_width As Long = 0) As String
        
    Dim ss  As String
    
    pasre_addr = ""
    
    ss = UCase(Trim(in_str))    '小文字⇒大文字変換
    If ss Like "0X*" Then
        ss = Mid(ss, 3)
    End If
    
    '上位の "0" を除去
    Do While Len(ss) > 1
        If Left(ss, 1) <> "0" Then
            Exit Do
        End If
        ss = Mid(ss, 2)
    Loop
    
    If Len(ss) > 8 Then
        Exit Function
    ElseIf Hex(CLng("&H" & ss)) <> ss Then ' １６進文字チェック
        Exit Function
    End If
    
    If out_width > 0 Then
        ss = Right(String(out_width, "0") & ss, out_width)  '桁数を調整
    End If
    
    pasre_addr = ss
End Function

'--------------------------------------------------------------
' 16進⇒ビット抽出
'--------------------------------------------------------------
Private Sub pasre_bit(in_str As String, out_ary() As Boolean)
    Dim ss  As String
    Dim cc  As String
    Dim ii  As Long
    Dim jj  As Long
    Dim vv  As Long
    
    ss = Right$(String(8, "0") & in_str, 8)  '桁数を補整
    
    ii = 31
    For jj = 1 To 8
        cc = Mid$(ss, jj, 1)
        vv = CLng("&H" & cc)
        
        out_ary(ii - 0) = IIf(vv And &H8, True, False)
        out_ary(ii - 1) = IIf(vv And &H4, True, False)
        out_ary(ii - 2) = IIf(vv And &H2, True, False)
        out_ary(ii - 3) = IIf(vv And &H1, True, False)
        ii = ii - 4
    Next jj
    
End Sub

'--------------------------------------------------------------
' ログファイル名 ⇒「被疑部品名」「被疑部品番号」
'--------------------------------------------------------------
Private Function detect_RepPartByLogname( _
        varRefTable As Variant, _
        ByVal name_str As String, _
        out_base As Range, _
        ByRef out_row As Variant) As Boolean
        
    Dim rngKeyTop As Range
    Dim booFound As Boolean
    Dim c1 As Range
    Dim c2 As Range
    
    Set rngKeyTop = varRefTable(7) 'ファイル名列
    
    If verbose_flag Then Call MyRangeSelect(rngKeyTop)
    
    ' ファイル名検索
    Set c1 = row_search(rngKeyTop, name_str)
    
    out_row = out_row + 1
    Set c2 = out_base.Rows(out_row)
    
    Call set_result(c2.Offset(0, d2_Addr_ofs), name_str, bWrapText:=False)
    Call set_result(c2.Offset(0, d2_Bit_ofs), "")
    Call set_result(c2.Offset(0, d2_BitName_ofs), "")
    
    If c1 Is Nothing Then
        Call set_error_mark(c2.Offset(0, d2_Addr_ofs))
        booFound = False
    Else
        Call set_result(c2.Offset(0, d2_RepairPart_ofs), c1.Offset(0, 1).Value)
        booFound = True
    End If
    
    detect_RepPartByLogname = booFound
End Function

'--------------------------------------------------------------
' 「data1」「data2」⇒「被疑部品名」「被疑部品番号」
'--------------------------------------------------------------
Private Function detect_RepairPart( _
        varRefTable As Variant, _
        ByVal data1_str As String, _
        ByVal data2_str As String, _
        out_base As Range, _
        ByRef out_row As Variant) As Boolean
        
    Dim intEndRow As Variant
    Dim rngAddrTop As Range
    Dim intRefBitCol As Long
    Dim intRefBitNameCol As Long
    Dim intRefTargetCol As Long
    Dim intRefPartsNoCol As Long
    Dim intRefPartsAdrCol As Long
    Dim rngNameTop As Range
    
    Dim intFound As Variant
    Dim booBitOn As Boolean
    
    Dim data2_ary(31)   As Boolean
    Dim i As Long
    Dim c1 As Range
    Dim c2  As Range
    Dim strParts(2) As String
    
    intEndRow = varRefTable(0)
    Set rngAddrTop = varRefTable(1) 'Addr列
    intRefBitCol = varRefTable(2)
    intRefBitNameCol = varRefTable(3)
    intRefTargetCol = varRefTable(4)
    intRefPartsNoCol = varRefTable(5)
    intRefPartsAdrCol = varRefTable(6)
    Set rngNameTop = varRefTable(1) 'ファイル名列
    
    If verbose_flag Then Call MyRangeSelect(rngAddrTop)
    
    intFound = 0
    booBitOn = False
    
    ' ADDRESS検索
    Set c1 = search_tab_rows(data1_str, rngAddrTop, intEndRow)
    
    ' BIT検索
    Call pasre_bit(data2_str, data2_ary)
    For i = 0 To 31
        If data2_ary(i) Then
            booBitOn = True
            
            Set c2 = c1
            Do While Not c2 Is Nothing
                If check_tab_bits(c2.EntireRow.Columns(intRefBitCol), i) Then
                    Exit Do
                End If
                Set c2 = search_tab_rows(data1_str, c2.Offset(1), intEndRow)
            Loop
            
            If c2 Is Nothing Then
                Erase strParts
            Else
                intFound = intFound + 1
                strParts(0) = Replace(Trim(c2.EntireRow.Columns(intRefPartsNoCol)), ",", "," & vbLf)
                strParts(1) = Replace(Trim(c2.EntireRow.Columns(intRefPartsAdrCol)), ",", "," & vbLf)
                strParts(2) = Replace(Trim(c2.EntireRow.Columns(intRefTargetCol)), ",", "," & vbLf)
            End If
            
            out_row = out_row + 1
            With out_base.Rows(out_row)
                Call set_result(.Offset(0, d2_Addr_ofs), data1_str)
                Call set_result(.Offset(0, d2_Bit_ofs), i)
            
                If c1 Is Nothing Then
                    Call set_error_mark(.Offset(0, d2_Addr_ofs))
                ElseIf c2 Is Nothing Then
                    Call set_error_mark(.Offset(0, d2_Bit_ofs))
                Else
                    Call set_result(.Offset(0, d2_BitName_ofs), Trim(c2.EntireRow.Columns(intRefBitNameCol).Value))
                    Call set_result(.Offset(0, d2_RepairPart_ofs), strParts(2))
                    Call set_result(.Offset(0, d2_RepairFigNo_ofs), strParts(0))
                    Call set_result(.Offset(0, d2_RepairPlace_ofs), strParts(1))
                End If
            End With
        End If
    Next i
    
    If booBitOn = False Then
        out_row = out_row + 1
        With out_base.Rows(out_row)
            Call set_result(.Offset(0, d2_Addr_ofs), data1_str)
            Call set_result(.Offset(0, d2_Bit_ofs), "")
            If c1 Is Nothing Then 'Addressが見つからない場合
                Call set_error_mark(.Offset(0, d2_Addr_ofs))
            Else
                Call set_error_mark(.Offset(0, d2_Bit_ofs))
            End If
        End With
    End If
    
    detect_RepairPart = IIf(intFound > 0, True, False)
End Function

Private Function search_bit( _
        rngStartCell As Range, _
        ByVal intEndRow As Variant, _
        ByVal intSearchBit As Long) As Long
        
    Dim c1  As Range
    
    Set c1 = rngStartCell
    Do While c1.Row <= intEndRow
        If check_tab_bits(Trim$(c1.Value), intSearchBit) Then
            search_bit = c1.Row
            Exit Function
        End If
            
        Set c1 = c1.Offset(1, 0)
    Loop
    
    search_bit = 0
End Function

'--------------------------------------------------------------
' テーブル検索
'--------------------------------------------------------------
Private Function search_tab_rows( _
        s_str As String, _
        tab_base As Range, _
        last_row As Variant) As Range
    
    Set search_tab_rows = row_search(tab_base, s_str)
End Function

Private Sub test_check_tab_bits()
    Debug.Print check_tab_bits("7:0", 0)
    Debug.Print check_tab_bits("7:0", 7)
    Debug.Print check_tab_bits("7:0", 8)
    
    Debug.Print check_tab_bits("0　〜　15", 0)
    Debug.Print check_tab_bits("0　〜　15", 15)
    Debug.Print check_tab_bits("0　〜　15", 16)
    Debug.Print check_tab_bits("15　〜　0", 1)
End Sub

Private Function check_tab_bits(ByVal s_str As String, ByVal s_value As Long) As Boolean
    Dim min_val As Long
    Dim max_val As Long
    Dim n As Long
    
    check_tab_bits = False
    
    s_str = Trim(MySpaceCharsReplace(s_str, " "))
    If s_str Like "Bit*" Then
        s_str = LTrim(Mid(s_str, 4))
        If s_str Like "[[]*[]]" Then
            s_str = Trim(Mid(s_str, 2, Len(s_str) - 2))
        End If
    End If
    
    n = 0
    If s_str Like "*:*" Then
        n = InStr(s_str, ":")
    ElseIf s_str Like "*〜*" Then
        n = InStr(s_str, "〜")
    End If
    If n > 0 Then
        min_val = get_IntVal(Left$(s_str, n - 1))
        max_val = get_IntVal(Mid(s_str, n + 1))
    ElseIf s_str Like "#*" Then
        min_val = get_IntVal(s_str)
        max_val = min_val
    Else
        min_val = -1
    End If
    
    If min_val < 0 Then
        Exit Function
    ElseIf max_val < 0 Then
        Exit Function
    ElseIf min_val > max_val Then
        n = min_val
        min_val = max_val
        max_val = n
    End If
    
    Select Case s_value
    Case Is < min_val
        Exit Function
    Case Is > max_val
        Exit Function
    End Select
    
    check_tab_bits = True
End Function

Private Function get_IntVal(ByVal s_str As String) As Long
    If IsNumeric(s_str) Then
        If Int(s_str) = s_str Then
            get_IntVal = Int(s_str)
            Exit Function
        End If
    End If
    
    get_IntVal = -1
End Function
