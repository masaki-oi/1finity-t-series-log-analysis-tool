Attribute VB_Name = "LogExtract"
'-------------------------------------------------------------
' All Rights Reserved, Copyright (C) FUJITSU LIMITED 2017
'
'   TECHINFO展開
'
'   更新日: 2017年 8月31日
'
'-------------------------------------------------------------
Option Explicit

Private Const sPasteLogName_base = "A1"   'swerr.logファイル名設定位置
Private Const sPasteLogText_base = "A2" 'ログテキスト貼り付け位置
Private Const sPasteLogHeader1 As String = "## TECHINFOファイル名"
Private Const sPasteLogHeader2 As String = "## TECHINFO解凍ログ"

Public Function TECHINFO展開_Execute( _
        strInputPath As String) As Boolean
        
    TECHINFO展開_Execute = False
    
    Const job_name = "TECHINFO展開"
    
    Dim logfile As String
    Dim logtext As String
    Dim strLogHeader As String
    Dim booResult As Boolean
    Dim intRetCode As Long
    
    If IsFolderName(strInputPath) Then
        logfile = ""
    Else
        logfile = strInputPath
    End If
    
    Call area_clear(99) '出力領域
    Call area_clear(3)  '日時指定クリア
    
    '解凍実行
    If LCase(logfile) Like "*.txt" Then
        strLogHeader = sPasteLogHeader1
        logtext = logfile
        booResult = True
    ElseIf LCase(logfile) Like "*.log" Then
        strLogHeader = sPasteLogHeader1
        logtext = logfile
        booResult = True
    Else
        strLogHeader = sPasteLogHeader2
        logtext = swerr_execute(strInputPath, intRetCode)
        If logtext = "" Then
            Exit Function
        End If
        booResult = IIf(intRetCode = 0, True, False)
    End If
        
    '解凍ログ貼り付け実行
    If swerrlog_paste(logtext, strLogHeader) = False Then
        booResult = False
    Else
        Call job_end(job_name, booResult)
    End If
    
    TECHINFO展開_Execute = booResult
End Function

Private Function swerr_execute( _
            ByVal strInputPath As String, _
            ByRef intRetCode As Long) As String
            
    Const job_name = "TECHINFOログ解凍"
    Dim WSH As Object
    Dim exefile As String
    Dim strOutFolder  As String
    Dim logtext As String
    Dim strCmd  As String
    Dim rc  As Long
    Dim strTmp As String
    Dim dtStart As Date
    Dim dtLogFile As Date
    Dim strOptions As String
    
    Call job_begin(job_name)
    
    rc = 2
    logtext = ""
    strOptions = " /A "
    
    '解凍種別を取得
    Dim varBladeName As Variant
    varBladeName = BladeName_pickup(省略可:=False)
    If IsEmpty(varBladeName) Then GoTo Exit_Proc
    If varBladeName = "T200" Then
        strOptions = strOptions & "/T200 "
    ElseIf varBladeName = "T700" Then
        strOptions = strOptions & "/T700 "
    End If

    'ツールと出力先のフルパスを取得
    strTmp = toolPara_get("SwerrTool", 省略可:=False)
    If strTmp = "" Then GoTo Exit_Proc
    
    exefile = MyFullPathGet(strTmp)
    If Not IsExistPath(exefile) Then
        MsgBox "'" & exefile & "'" & vbCrLf _
            & "が存在しません。設定を確認して下さい。", vbCritical
        GoTo Exit_Proc
    End If
    
    strTmp = toolPara_get("SwerrOut", 省略可:=False)
    If strTmp = "" Then GoTo Exit_Proc
    
    strOutFolder = MyFullPathGet(strTmp)
    If Not IsExistPath(strOutFolder) Then
        MsgBox "'" & strOutFolder & "'" & vbCrLf _
            & "が存在しません。設定を確認して下さい。", vbCritical
        GoTo Exit_Proc
    ElseIf Not IsFolderName(strOutFolder) Then
        MsgBox "'" & strOutFolder & "'" & vbCrLf _
            & "はフォルダではありません。設定を確認して下さい。", vbCritical
        GoTo Exit_Proc
    End If
    
    'DOS窓を開く
    Set WSH = CreateObject("WScript.shell")
    'コマンドを作る
    strCmd = """" & exefile & """" & strOptions & """" & strInputPath & """ """ & strOutFolder & """"
    
    '実行開始日時を取得する
    dtStart = Now
    
    'コマンドをDOS窓に投入
    On Error GoTo Exec_Fail
    rc = WSH.Run(strCmd, IIf(True, vbNormalFocus, vbMinimizedNoFocus), True)
    On Error GoTo 0
    ':: rc【戻り値】
    ':: 3: 実行ログ初期化前にエラー発生
    ':: 2: 実行ログ初期化後にエラー発生
    ':: 1: TECHINFOログファイルなし
    ':: 0: TECHINFOログファイル抽出 OK
    
    Select Case rc
    Case 0 To 2
        '実行ログファイルをチェック
        logtext = strOutFolder & "\Extract.log"
        If Not IsExistPath(logtext) Then
            'ファイルができていない
            Call MsgBox(job_name & "実行ログファイル" & vbCrLf _
                  & " [" & logtext & "]" & vbCrLf _
                  & "ができていません。" & vbCrLf & vbCrLf _
                  & "実行中に異常が発生していると思われます。" _
                  , vbExclamation)
            rc = 3
        ElseIf MyFileDateGet(logtext, dtLogFile) = False Then
            'ファイルの異常
            If MsgBox(job_name & "実行ログファイル" & vbCrLf _
                    & " [" & logtext & "]" & vbCrLf _
                    & "の更新時刻が取得できません。" & vbCrLf & vbCrLf _
                    & "実行中に異常が発生していると思われますが、" & vbCrLf _
                    & "処理を続けますか？" _
                    , vbExclamation + vbYesNo) = vbNo Then
                rc = 3
            End If
        ElseIf dtLogFile < dtStart Then
            'ファイルが古い
            Call MsgBox(job_name & "実行ログファイル" & vbCrLf _
                  & " [" & logtext & "]" & vbCrLf _
                  & "が更新されていません。" & vbCrLf & vbCrLf _
                  & "実行中に異常が発生していると思われます。" _
                  , vbExclamation)
            rc = 3
        End If
        
    Case 3  '実行ログ初期化前にエラー発生
        
    Case Else '想定外の戻り値
        Call MsgBox(job_name & "実行結果が判定できません。" & vbCrLf & vbCrLf _
                  & "実行中に異常が発生していると思われます。" & vbCrLf & vbCrLf _
                  & "  ※実行経過表示のウィンドウを[X](閉じる)ボタンで" & vbCrLf _
                  & "    終了させた場合も異常になります。" _
                  , vbExclamation)
        rc = 3
    End Select
    
Exit_Proc:
    On Error GoTo 0
    
    Call job_end(job_name, rc = 0)
    
    Set WSH = Nothing
    
    intRetCode = rc
    swerr_execute = logtext
    Exit Function
    
Exec_Fail:
    MsgBox "'" & exefile & "'" & vbCrLf _
        & "の起動に失敗しました" & vbCrLf & vbCrLf _
        & Err.Description, vbCritical
    
    Resume Exit_Proc
    
End Function

'解凍ログの読込、貼付け
Private Function swerrlog_paste( _
        strFullPath As String, _
        strLogHeader As String) As Boolean
        
    Const job_name = "SWERR解凍ログ貼付け"
    Dim ret_value   As Boolean
    Dim out_ws  As Worksheet
    Dim intFF   As Long  ' FreeFile値
    Dim buf As String
    Dim base_cel    As Range
    Dim c1  As Range
    
    ret_value = False
    
    Call job_begin(job_name)
        
    Set out_ws = GetWorkSheetByName(sPasteLog_shname, ThisWorkbook)
    out_ws.Range(sPasteLogName_base).Value = strLogHeader
    
    If strFullPath = "" Then
        GoTo Exit_Proc
    End If
    
    Set base_cel = out_ws.Range(sPasteLogText_base)
    Set out_ws = Nothing
    
    If strLogHeader = sPasteLogHeader1 Then
        base_cel.NumberFormatLocal = "@"
        base_cel.Value = strFullPath
        ret_value = True
        GoTo Exit_Proc
    End If
    
    On Error GoTo Read_Fail
    intFF = FreeFile
    Open strFullPath For Input As #intFF
        Set c1 = base_cel
        Do Until EOF(intFF)
            Line Input #intFF, buf
            If Trim$(buf) <> "" Then
                    '注) swerr.exeの出力は、改行コードが CrLfCr, CrCrLf 等になっており、空行が多いので除去。
                buf = RTrim$(Replace$(buf, vbTab, "    ")) 'TAB文字を削除
                c1.NumberFormatLocal = "@"
                c1.Value = buf
                Set c1 = c1.Offset(1)
             End If
        Loop
    Close #intFF
    On Error GoTo 0
    
    'ファイルの中身がなかった（=ROW用のカウンタ(n)が初期値(0)のまま)
    If base_cel.Row = c1.Row Then
        c1.Value = "ログファイルが空です。"
        GoTo Exit_Proc
    End If
    
    ret_value = True
    
Exit_Proc:
    On Error GoTo 0
    Call job_end(job_name, ret_value)
    
    Set base_cel = Nothing
    Set c1 = Nothing
    Set out_ws = Nothing
    
    swerrlog_paste = ret_value
    Exit Function
    
Read_Fail:
    MsgBox "'" & strFullPath & "'" & vbCrLf _
        & "の読み込みに失敗しました" & vbCrLf & vbCrLf _
        & Err.Description, vbCritical
    
    Resume Exit_Proc
End Function

'解凍ログからファイルリスト取得
Public Function swerrlog_list( _
        ByRef varResult() As Variant, _
        wsList As Worksheet, _
        Optional ByVal NoMsg As Boolean = False) As Long
    
    Dim input_fh    As MySheetTextInput
    Dim rngBase As Range
    Dim rngEnd  As Range
    
    Dim intCount As Long
    Dim intType  As Long
    Dim strText   As String
    Dim strPath(2)  As String
    
    intCount = 0
        
    If wsList Is Nothing Then GoTo Exit_Proc
    
    Set rngEnd = get_lastDataCell(wsList)
    Set rngBase = wsList.Range(sPasteLogName_base)
    Call MyRangeSelect(rngBase)
        
    '入力データチェック
    If rngEnd.Address <> rngBase.Address Then
    ElseIf IsSpaceString(rngBase.Value) Then
        If Not NoMsg Then
            Call MyRangeSelect(rngBase.Offset(1, 0))
            MsgBox "ログテキスト領域にデータがありません"
        End If
        GoTo Exit_Proc
    End If
    
    Set input_fh = New MySheetTextInput
    If input_fh.Initialize(wsList.Range(rngBase, rngEnd)) = False Then
        'オープンfail
        GoTo Exit_Proc
    End If
    
    intType = 0
    Do While input_fh.record_read()
        strText = Trim(input_fh.LineText)
        
        If IsSpaceString(strText) Then
            ' 空行は無視
        ElseIf intType = 0 Then
            If strText = sPasteLogHeader1 Then
                intType = 1    'ログファイル名の指定
                
            ElseIf strText = sPasteLogHeader2 Then
                intType = 2    '解凍ログの始まり
                strPath(0) = ""
            Else
                Dim c1 As Range
                intType = 3    'ログテキストの始まり
                Set rngBase = rngBase.Offset(input_fh.LineNo - 1)
                Do While rngBase.Column < rngEnd.Column
                    Set c1 = rngBase.Cells(1)
                    Do While True
                        If c1.HasFormula Then
                            If Not IsSpaceString(c1.Formula) Then
                                Exit Do
                            End If
                        ElseIf Not IsSpaceString(c1.Value) Then
                            Exit Do
                        ElseIf c1.Row >= rngEnd.Row Then
                            Set c1 = Nothing
                            Exit Do
                        End If
                        Set c1 = c1.Offset(1)
                    Loop
                    If Not c1 Is Nothing Then
                        Exit Do
                    End If
                    Set rngBase = rngBase.Offset(, 1)
                Loop
                ReDim varResult(0)
                Set varResult(0) = wsList.Range(rngBase, rngEnd)
                intCount = 1
                Exit Do
            End If
            
        ElseIf intType = 1 Then
            ReDim Preserve varResult(intCount)
            varResult(intCount) = strText
            intCount = intCount + 1
                
        ElseIf Left(strText, 1) = "#" Then
            strText = Trim$(Mid(strText, 2))
            If strText Like "OUTPUT:*" Then
                strPath(0) = Trim(Mid(strText, 8))
            End If
        Else
            Select Case Left(strText, 2)
            Case "F "
                strPath(1) = Trim(Mid(strText, 3))
            Case "x "
                ReDim Preserve varResult(intCount)
                strPath(2) = Trim(Mid(strText, 3))
                varResult(intCount) = encode_extractPath(strPath)
                intCount = intCount + 1
            End Select
        End If
    Loop

    If (intType = 0) Or (intCount < 1) Then
        If Not NoMsg Then
            Call MyRangeSelect(rngBase)
            MsgBox "有効なデータがありません"
        End If
    End If
    
Exit_Proc:
    Set rngBase = Nothing
    Set rngEnd = Nothing
    Set input_fh = Nothing

    swerrlog_list = intCount
End Function

Private Function encode_extractPath(strPath() As String) As String
    encode_extractPath = Join(strPath, vbCrLf)
End Function

Public Function decode_extractPath(ByVal strData As String) As String
    decode_extractPath = Replace(strData, vbCrLf, "\")
End Function

Public Function split_extractPath(ByVal strData As String) As Variant
    split_extractPath = Split(strData, vbCrLf)
End Function

