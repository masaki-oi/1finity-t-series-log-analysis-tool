Attribute VB_Name = "ResultSave"
'-------------------------------------------------------------
' All Rights Reserved, Copyright (C) FUJITSU LIMITED 2017
'
'   実行結果の保存
'
'   更新日: 2017年 3月27日
'
'-------------------------------------------------------------
Option Explicit

Public Function 実行結果保存_Execute( _
        DoClose As Boolean) As Boolean
        
    Const strJobName As String = "集計結果保存"
    Dim booResult As Boolean
    
    Dim strSrcSheetName(2)  As String '保存対象シート名
    Dim strRepoBookPath As String
    Dim wbSrc As Workbook
    Dim wbRepo As Workbook
    Dim strName As String
    Dim i As Long
    
    booResult = False
    
    ' 保存するシートをチェック
    strSrcSheetName(0) = control_shname
    strSrcSheetName(1) = sPasteLog_shname
    strSrcSheetName(2) = deftab_shname
    Set wbSrc = ThisWorkbook
    For i = 0 To 2
        strName = strSrcSheetName(i)
        If GetWorkSheetByName(strName, wbSrc) Is Nothing Then
            GoTo Error_Exit
        End If
    Next i
    
    ' 出力ファイル名取得
    strName = toolPara_get("AutoSaveFile")
    If strName = "" Then
        strRepoBookPath = ""
    Else
        strRepoBookPath = MyFullPathGet(strName)
        If MyOpenBookCheck(wbRepo, strRepoBookPath, ReuseEnable:=False) = False Then
            Set wbRepo = Nothing
            GoTo Error_Exit
        End If
    End If
    
    ' 前処理
    Call job_begin(strJobName)
    
    ' コピー実行
    If ResultSheetSave(wbRepo, wbSrc, strSrcSheetName) > 0 Then
        GoTo Error_Exit
    End If
    
    If strRepoBookPath <> "" Then
        On Error Resume Next
        
        Application.DisplayAlerts = True
        wbRepo.SaveAs Filename:=strRepoBookPath
        If Err.Number <> 0 Then
            Err.Clear
            GoTo Error_Exit
        End If
        
        If DoClose Then
            wbRepo.Close
            If Err.Number <> 0 Then
                Call MsgBox(Err.Description, vbCritical)
                Err.Clear
                GoTo Error_Exit
            End If
            Set wbRepo = Nothing
        End If
        
        On Error GoTo 0
    End If
    
    booResult = True
    
Error_Exit:
    On Error GoTo 0
    
    Call job_end(strJobName, booResult)
    
    If Not wbRepo Is Nothing Then
        wbRepo.Activate
    End If
    
    Erase strSrcSheetName
    
    実行結果保存_Execute = booResult
End Function

Private Function ResultSheetSave( _
        ByRef wbDst As Workbook, _
        wbSrc As Workbook, _
        strSrcSheetName() As String) As Long
        
    On Error GoTo Error_Exit
    wbSrc.Activate
    wbSrc.Worksheets(strSrcSheetName).Copy
    Set wbDst = ActiveWorkbook
    If wbDst.FullName = wbSrc.FullName Then
        GoTo Error_Exit
    ElseIf Not (wbDst.Name Like "Book#*") Then
        GoTo Error_Exit
    End If
    On Error GoTo 0
    
    ' マクロ等を除去
    If ResultSheet_Clean(wbDst) = False Then
        ResultSheetSave = 1 '除去処理でエラー
    Else
        ResultSheetSave = 0 '正常終了
    End If
    
    Exit Function
    
Error_Exit:
    Call MsgBox("シートの保存に失敗しました" & vbCrLf & vbCrLf _
                   & IIf(Err.Number = 0, "", Err.Description), vbCritical)
    Err.Clear
    
    Set wbDst = Nothing
    ResultSheetSave = 2
End Function

' マクロ起動ボタン等を除去
Private Function ResultSheet_Clean(wbTarget As Workbook) As Boolean
        
    Dim wsTmp As Worksheet
    Dim shpTmp As Shape
    Dim strTmp As String
    Dim intErrCount As Long
    Dim i As Variant
    
    intErrCount = 0
    
    For Each wsTmp In wbTarget.Worksheets
        'シート保護解除
        On Error Resume Next
        wsTmp.Unprotect
        If Err.Number <> 0 Then
            Call MsgBox(wsTmp.Name & "シートの保護が解除できません")
            intErrCount = intErrCount + 1
            Err.Clear
            GoTo Exit_Proc
        End If
        On Error GoTo 0
        
        '入力規則削除
        If MyObjectDelete(wsTmp.Cells.Validation) = False Then
            intErrCount = intErrCount + 1
        End If
        
        'マクロ起動ボタンを除去
        For i = wsTmp.Shapes.Count To 1 Step -1
            Set shpTmp = wsTmp.Shapes(i)
            On Error Resume Next
            strTmp = shpTmp.OnAction
            If (strTmp <> "") And (Err.Number = 0) Then
                If MyObjectDelete(shpTmp) = False Then
                    intErrCount = intErrCount + 1
                End If
            End If
            On Error GoTo 0
        Next i
        
        Call MyMacroCodeDelete(wsTmp)
    Next wsTmp
    
Exit_Proc:
    On Error GoTo 0
    ResultSheet_Clean = IIf(intErrCount = 0, True, False)
End Function

'シートマクロの除去
Public Sub MyMacroCodeDelete(objWS As Worksheet)
  
    On Error GoTo Delete_Fail
    Dim intCount As Variant
    With objWS.Parent.VBProject.VBComponents(objWS.CodeName).CodeModule
        intCount = .CountOfLines
        If intCount <> 0 Then
            .DeleteLines 1, intCount
        End If
    End With
    Exit Sub
    
Delete_Fail:
'    Call MsgBox(objWS.Name & "のシートマクロが削除できません。" & vbCrLf & vbCrLf _
'                & Err.Description, vbCritical)
End Sub


Private Function MyObjectDelete(objTarget As Variant) As Boolean
    Application.DisplayAlerts = False
    On Error Resume Next
    objTarget.Delete
    If Err.Number <> 0 Then
        Call MsgBox(objTarget.Name & "が削除できません。" & vbCrLf & vbCrLf _
                        & Err.Description, vbCritical)
        MyObjectDelete = False
    Else
        MyObjectDelete = True
    End If
    Err.Clear
    Application.DisplayAlerts = True
End Function

