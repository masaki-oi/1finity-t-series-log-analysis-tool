Attribute VB_Name = "LogParse"
'-------------------------------------------------------------
' All Rights Reserved, Copyright (C) FUJITSU LIMITED 2017
'
'   ログ解析
'
'   更新日: 2017年 8月31日
'
'-------------------------------------------------------------
Option Explicit

Public Enum MyLogFileType
    LogFileUnknown = 0
    LogFileUnitFLT = 1
    LogFileFLT = 2
    T700BladeFLT = 3
    T700PiuFLT = 4
    T700QsfpFLT = 5
End Enum


Public Function ログ解析_Execute() As Variant
    Const job_name = "ログ解析"
    
    Dim ret_value   As Variant
    Dim varList()   As Variant
    Dim strData As String
    Dim strPath As String
    Dim intCount As Variant
    Dim input_fh As Variant
    Dim i As Long
    Dim wsList  As Worksheet
    Dim wsOut   As Worksheet
    Dim output_base As Range
    Dim last_output_row  As Range
    
    Call job_begin(job_name)
    
    '出力領域クリア
    Call area_clear(1)
    Call area_clear(2)
    
    ret_value = -1
    
    Set wsOut = GetWorkSheetByName(control_shname, ThisWorkbook)
    
    Set output_base = wsOut.Cells(data_base_line, d1_data_base_col)
    Set last_output_row = output_base
    
    Set wsList = GetWorkSheetByName(sPasteLog_shname, ThisWorkbook)
    If wsList Is Nothing Then GoTo Exit_Proc
    Call MySheetActivate(wsList)
    Call MyScreenUpdate
    
    If swerrlog_list(varList, wsList) < 1 Then
        ret_value = 0
        GoTo Exit_Proc  ' 有効データなし
    End If

    Debug.Print "(ログ解析_Execute):"
    For i = 0 To UBound(varList)
      If TypeName(varList(i)) <> "Range" Then
        Debug.Print "varList(" & i & ") = " & vbCrLf & varList(i)
      End If
    Next i

    intCount = 0
    For i = 0 To UBound(varList)
        If TypeName(varList(i)) = "Range" Then
            Set input_fh = New MySheetTextInput
            If input_fh.Initialize(varList(i), Verbose:=False) = False Then
                'オープンfail
            ElseIf parse_LogFile(last_output_row, input_fh) > 0 Then
                intCount = intCount + 1
            End If
        Else
            strData = varList(i)
            strPath = decode_extractPath(strData)
            Set input_fh = New MyUnixTextInClass
            If input_fh.Initialize(strPath, Verbose:=False) = False Then
                'オープンfail
            ElseIf parse_LogFile(last_output_row, input_fh, strData, i + 1) > 0 Then
                intCount = intCount + 1
            End If
        End If
        Set input_fh = Nothing
        DoEvents
    Next i
    Erase varList
    
    If intCount = 0 Then
        ret_value = 0
        Call MsgBox("有効なログデータがありません", vbCritical)
        GoTo Exit_Proc
    End If
    
    '降順に並べ替え
    Dim c2 As Range
    Set c2 = output_base.Resize(1, d1_data_max_ofs + 1)
    Call MyRangeSelect(c2)   '注）Selectは不要だが、ソート範囲を目視で確認するため。
    intCount = last_output_row.Row - output_base.Row
    
    With wsOut.Sort
        With .SortFields
            .Clear
            .Add Key:=output_base.Offset(0, d1_Date_ofs), Order:=xlDescending
            .Add Key:=output_base.Offset(0, d1_Time_ofs), Order:=xlDescending
            .Add Key:=output_base.Offset(0, d1_ID_ofs), Order:=xlAscending
        End With
        
        .SetRange c2.Resize(last_output_row.Row - c2.Row + 1)
        .Header = xlYes
        .Orientation = xlTopToBottom
        .Apply
    End With
    
    'リナンバー
    Dim intNo As Variant
    Set c2 = output_base.Offset(0, d1_No_ofs)
    For intNo = 1 To intCount
        Set c2 = c2.Offset(1)
        c2.Value = CStr(intNo)
    Next intNo
    
    Call MyRangeSelect(output_base)
    
    ret_value = intCount
    
Exit_Proc:
    Call job_end(job_name, ret_value > 0)
    
    ログ解析_Execute = ret_value
End Function


Public Function ログ解析結果絞込み_Execute() As Variant
    Const job_name As String = "ログ解析結果絞込み"
    Dim ret_code   As Variant
    Dim wsTarget   As Worksheet
    Dim date_lower  As Variant
    Dim date_upper  As Variant
    Dim dtLower  As Date
    Dim dtUpper  As Date
    
    Dim i As Variant
    Dim intRowsCount As Variant
    Dim c0 As Range
    Dim c1 As Range
    Dim No_str    As String
    Dim num1    As Long
    Dim num2    As Long
    
    ' 抽出範囲指定取り出し
    If date_get(date_lower, date_upper) = False Then
        Call MsgBox("抽出範囲日時の指定がありません")
        ret_code = -1
        GoTo Exit_Proc
    End If
    dtLower = date_lower
    dtUpper = date_upper
    
    Call job_begin(job_name)
    
    '依存出力領域クリア
    Call area_clear(2)
    
    Set wsTarget = GetWorkSheetByName(control_shname, ThisWorkbook)
    
    Set c0 = wsTarget.Columns(d1_data_base_col).Rows(data_base_line)
    Set c1 = c0.Offset(1)
    intRowsCount = get_lastDataCell(wsTarget).Row - c1.Row + 1
    
    num1 = 0
    num2 = 0
    For i = 1 To intRowsCount
        No_str = c1.Offset(0, d1_No_ofs)
        If No_str = "" Then
            Exit For
        End If
        
        num1 = num1 + 1
        If date_check_d1(c1, dtLower, dtUpper) Then
            num2 = num2 + 1
            Set c0 = c1
        Else
            c1.EntireRow.Delete
        End If
        Set c1 = c0.Offset(1)
    Next i
    
    ret_code = num2 '検出数を返す。
    If num1 = 0 Then '検索対象が 0 件の場合
        Call MsgBox("ログ解析結果が空です")
    End If
       
Exit_Proc:
    Dim strMsg  As String
    Select Case ret_code
    Case Is > 0
        Call MyRangeSelect(wsTarget.Cells(data_base_line, d1_data_base_col))
        strMsg = "完了"
    Case 0
        strMsg = "終了(有効データ無し)"
        
    Case Is < 0
        strMsg = "中止(設定エラー)"
    End Select
        
    Call job_end(job_name, strMsg)
    
    Set c0 = Nothing
    Set c1 = Nothing
    Set wsTarget = Nothing
    
    ログ解析結果絞込み_Execute = ret_code
    
End Function


Private Function date_check_d1( _
    rngTarget As Range, _
    date_lower As Date, _
    date_upper As Date) As Boolean
    
    Dim booResult As Boolean
    Dim dtRecord As Date
    Dim varDate As Variant
    Dim varTime As Variant
    
    On Error Resume Next
    With rngTarget
        varDate = .Offset(0, d1_Date_ofs)
        varTime = .Offset(0, d1_Time_ofs)
    End With
    
    If VarType(varDate) <> vbDate Then
        varDate = CDate(varDate)
    End If
    If Not IsDate(varDate & " " & CStr(TimeValue(Now))) Then
        varDate = ""
    End If
    If Err.Number <> 0 Then
        varDate = ""
        Err.Clear
    End If
    
    If VarType(varDate) <> vbDate Then
        varTime = ""
    End If
    
    If VarType(varTime) <> vbDate Then
        varTime = CDate(varTime)
    End If
    If Not IsDate(varDate & " " & varTime) Then
        varTime = ""
    End If
    If Err.Number <> 0 Then
        varTime = ""
        Err.Clear
    End If
    
    On Error GoTo 0
        
    If VarType(varDate) <> vbDate Then
        Call set_error_mark(rngTarget.Offset(0, d1_Date_ofs))
        booResult = True
        
    ElseIf VarType(varTime) <> vbDate Then
        Call set_error_mark(rngTarget.Offset(0, d1_Time_ofs))
        dtRecord = varDate
        booResult = date_check(dtRecord, DateValue(date_lower), DateValue(date_upper))
    Else
        dtRecord = varDate + varTime
        booResult = date_check(dtRecord, date_lower, date_upper)
    End If
    
    date_check_d1 = booResult
End Function


' ログテキストの種別を判定
Public Function GetLogFileType( _
        Optional input_fh As Variant, _
        Optional ByVal Filename As String = "") As MyLogFileType
        
    Dim log_type    As MyLogFileType
    Dim log_text    As String
    Dim strName As String
    
    strName = Filename
    If strName <> "" Then
        strName = MyFileNameGet(strName)
    End If
    
    log_type = LogFileUnknown
    If strName Like "*_flt.txt" Then
        log_type = LogFileUnitFLT

    ElseIf strName Like "slot*.txt" Then
        log_type = LogFileFLT

    ElseIf strName Like "blade_FLT_*.log" Then
        log_type = T700BladeFLT

    ElseIf strName Like "PIU*_FLT_*.log" Then
        log_type = T700PiuFLT

    ElseIf strName Like "Slot*QSFP*_FLT_*.log" Then
        log_type = T700QsfpFLT

    ElseIf strName = "" Then
        Dim start_lno As Variant
        Do While input_fh.record_read()
            log_text = input_fh.LineText
            If IsSpaceString(log_text) Then
            ElseIf log_text Like "Date:*" Then
                start_lno = input_fh.LineNo
            Else
                Exit Do
            End If
        Loop
        If log_text Like "Unit Type*" Then
            log_type = LogFileUnitFLT
            
        ElseIf log_text Like "Slot Number:*" Then
            log_type = LogFileFLT
        End If
        
        If log_type = LogFileUnknown Then
            input_fh.Activate
            Call MsgBox("ログテキストの形式が不正です", vbCritical)
        Else
            Call input_fh.seek_line(start_lno - 1)
        End If
    End If
    
    GetLogFileType = log_type
End Function


Private Function parse_LogFile( _
        ByRef last_output_row As Range, _
        input_fh As Variant, _
        Optional ByVal strFileName As String = "", _
        Optional ByVal strFileID As String = "") As Long
        
    Dim ret_value   As Long
    Dim log_type    As MyLogFileType

    Debug.Print "(parse_LogFile):strFileName = " & strFileName & vbCrLf & _
                " strFileID = " & strFileID

    ' ログテキストの種別を判定
    log_type = GetLogFileType(input_fh, Filename:=decode_extractPath(strFileName))
    Select Case log_type
    Case LogFileUnitFLT
        ret_value = parse_UnitFLTLOG(last_output_row, _
                                     input_fh, _
                                     strFileName, _
                                     "FLT" & strFileID)
    Case LogFileFLT
        ret_value = parse_FLTLOG(last_output_row, _
                                 input_fh, _
                                 strFileName, _
                                 strFileID & "#")
    Case T700BladeFLT
        ret_value = parse_T700_BladeFLTLOG(last_output_row, _
                                           input_fh, _
                                           strFileName, _
                                           strFileID & "#")
    Case T700PiuFLT
        ret_value = parse_T700_PiuFLTLOG(last_output_row, _
                                         input_fh, _
                                         strFileName, _
                                         strFileID & "#")
    Case T700QsfpFLT
        ret_value = parse_T700_QsfpFLTLOG(last_output_row, _
                                          input_fh, _
                                          strFileName, _
                                          strFileID & "#")
    Case Else
        ret_value = -1
    End Select

    parse_LogFile = ret_value
End Function


Public Function parse_UnitFLTLOG( _
        ByRef last_output_row As Range, _
        input_fh As Variant, _
        ByVal strNameData As String, _
        Optional ByVal ID_str As String = "") As Long
        
    Dim ret_value   As Variant
    
    Dim log_text    As String
    Dim date_lower  As Variant
    Dim date_upper  As Variant
    Dim date_record As Date
    Dim date_val    As Variant
    Dim time_val    As Variant
    
    Dim strKey As String
    Dim strStatus As String
    
    ret_value = 0
    
    strStatus = input_fh.SourceName & " 読み込み中..."
    If Len(strStatus) > 50 Then
        strStatus = "〜" & Right(strStatus, 48)
    End If
    Application.StatusBar = strStatus
    
    ' 抽出範囲指定取り出し
    Call date_get(date_lower, date_upper)
        
    '------------------------------
    ' 日時情報切り出し
    '------------------------------
    Dim strMsg As String
    Dim strBuf As Variant
    Dim intStep As Long
    
    date_val = Null
    strMsg = ""
    
    strKey = "Date:"
    intStep = 0
    Do While input_fh.record_read()
        log_text = input_fh.LineText
        If Not IsSpaceString(log_text) Then
            intStep = intStep + 1
            
            If Left(log_text, Len(strKey)) <> strKey Then
                strMsg = "「" & strKey & "」行無し"
                Exit Do
            End If
            
            Select Case intStep
            Case 1
                If parse_LogDate(Trim(Mid(log_text, Len(strKey) + 1)), date_record) = False Then
                    strMsg = "日時形式不正"
                Else
                    date_val = DateValue(date_record)
                    time_val = TimeValue(date_record)
                    If date_check(date_record, date_lower, date_upper) = False Then  '範囲外の日付の場合
                        GoTo Exit_Proc '出力対象外
                    End If
                End If
                
                strKey = "Unit Type  :"
            Case 2
                strKey = "Slot Number:"
            Case 3
                Exit Do
            End Select
        End If
    Loop
    If intStep = 0 Then
        strMsg = "空ファイル"
    End If
    
    ret_value = 1
    
    '------------------------------
    ' 出力シートに設定
    '------------------------------
    Dim c2 As Range
    Dim strName As String
    strBuf = split_extractPath(strNameData)
    strName = Replace(strBuf(2), "\", "/")
    
    Set c2 = last_output_row.Offset(1)
    Call parse_result_set(c2, 1, ID_str, date_val, time_val, _
                          "", "", other_info:=strName, NG_msg:=strMsg)
    
    Erase strBuf
    
    Set last_output_row = c2
    
Exit_Proc:
    parse_UnitFLTLOG = ret_value
End Function


Private Function parse_FLTLOG( _
        ByRef last_output_row As Range, _
        input_fh As Variant, _
        ByVal strNameData As String, _
        Optional ByVal ID_base_str As String = "") As Long
        
    parse_FLTLOG = -1
    
    Dim c2  As Range
    Dim intCount   As Long '有効データ数カウンタ
    Dim log_text   As String
    Dim strBuf()    As String
    
    Dim ID_str As String
    Dim data1_str   As String
    Dim data2_str   As String
    Dim addr_val As Variant
    
    Dim ss  As String
    Dim i As Long
    Dim date_lower  As Variant
    Dim date_upper  As Variant
    Dim date_record As Date
    Dim date_val    As Variant
    Dim time_val    As Variant
    Dim intStep     As Long
    Dim strKey As String
    Dim strStatus As String
    Dim strMsg() As String
    Dim intMsgCount As Long
    ReDim strMsg(0)
    
    '解析種別を取得
    Dim varBladeName As Variant
    varBladeName = BladeName_pickup(省略可:=False)
    If IsEmpty(varBladeName) Then Exit Function
    
    ' 抽出範囲指定取り出し
    Call date_get(date_lower, date_upper)
    
    intCount = 0
    
    strStatus = input_fh.SourceName & " 読み込み中..."
    If Len(strStatus) > 50 Then
        strStatus = "〜" & Right(strStatus, 48)
    End If
    Application.StatusBar = strStatus
    
    '------------------------------
    ' 日時情報切り出し
    '------------------------------
    date_val = Null
    
    strKey = "Date:"
    intStep = 0
    Do While input_fh.record_read()
        log_text = input_fh.LineText
        If Not IsSpaceString(log_text) Then
            If Left(log_text, Len(strKey)) <> strKey Then
                strMsg(0) = "「" & strKey & "」行無し"
                intMsgCount = 1
                intStep = 1
            ElseIf parse_LogDate(Trim(Mid(log_text, Len(strKey) + 1)), date_record) = False Then
                strMsg(0) = "日時形式不正"
                intMsgCount = 1
                intStep = 2
            Else
                date_val = DateValue(date_record)
                time_val = TimeValue(date_record)
                If date_check(date_record, date_lower, date_upper) = False Then  '範囲外の日付の場合
                    GoTo Exit_Proc '出力対象外
                End If
                intStep = 3
            End If
            Exit Do
        End If
    Loop
    
    If intStep = 0 Then
        strMsg(0) = "空ファイル"
        intMsgCount = 1
        intCount = 1
        
    ElseIf intStep > 0 Then
        If intStep > 1 Then
            Call input_fh.record_read
        End If
        
        '------------------------------
        ' FLT情報開始位置検出
        '------------------------------
        strKey = "DAT file:"
        Do
            log_text = input_fh.LineText
            If Left(log_text, Len(strKey)) = strKey Then
                intStep = 4
                Exit Do
            End If
        Loop While input_fh.record_read()
        If intStep <> 4 Then
            ReDim Preserve strMsg(intMsgCount)
            strMsg(intMsgCount) = "「" & strKey & "」行無し"
            intMsgCount = intMsgCount + 1
        End If
    End If
    
    If intStep <> 4 Then
    ElseIf varBladeName = "T200" Then
        strKey = "c -- CDEC-CPLD --"
        Do While input_fh.record_read()
            log_text = input_fh.LineText
            log_text = MySpaceCharsReplace(log_text, " ")
            If Left(log_text, Len(strKey)) = strKey Then
                intStep = 5
                Exit Do
            End If
        Loop
        If intStep <> 5 Then
            ReDim Preserve strMsg(intMsgCount)
            strMsg(intMsgCount) = "「" & strKey & "」行無し"
            intMsgCount = intMsgCount + 1
        End If
    Else
        intStep = 5
    End If
    
    '------------------------------
    ' 出力シートに設定
    '------------------------------
    Set c2 = last_output_row
    
    If intStep <> 5 Then
        Set c2 = c2.Offset(1)
        intCount = intCount + 1
        ID_str = ID_base_str & input_fh.LineNo
        ss = IIf(intMsgCount > 0, Join(strMsg, vbCrLf), "")
        Call parse_result_set(c2, intCount, ID_str, date_val, time_val, _
                        "", "", NG_msg:=ss)
    Else
        'レジスタ情報解析
        Do While input_fh.record_read()
            log_text = input_fh.LineText
            log_text = MySpaceCharsReplace(log_text, " ")
            log_text = RTrim(log_text)
            Select Case Left(log_text, 1)
            Case "", "c"
                GoTo Continue
            Case " "    '継続行の場合 ※T200対応
                i = 1
            Case Else
                i = 2
            End Select
            
            'アドレス検出
            strBuf = Split(log_text, " ")
            If UBound(strBuf) < i Then
                GoTo Continue
            ElseIf Not strBuf(i) Like "*:" Then
                GoTo Continue
            End If
            data1_str = strBuf(i)
            data1_str = Left(data1_str, Len(data1_str) - 1)
            addr_val = pasre_addr(data1_str)
            If addr_val = "" Then
                GoTo Continue
            End If
            addr_val = CDec("&H" & addr_val)
            
            'レジスタ値処理
            Do While i < UBound(strBuf)
                i = i + 1
                data2_str = strBuf(i)

                If Replace(data2_str, "0", "") <> "" Then
                    Set c2 = c2.Offset(1)
                    intCount = intCount + 1
                    ID_str = ID_base_str & input_fh.LineNo
                    ss = IIf(intMsgCount > 0, Join(strMsg, vbCrLf), "")
                    Call parse_result_set(c2, intCount, ID_str, date_val, time_val, _
                            data1_str, data2_str, NG_msg:=ss)
                End If
                
                addr_val = addr_val + 4
                data1_str = LCase(Hex(addr_val))
            Loop
Continue:
        Loop
    End If
    
    Erase strBuf
    
    '------------------------------
    ' IDの桁数調整
    '------------------------------
    Dim j As Long
    Dim k As Long
    Dim c1 As Range
    Set c1 = last_output_row.Offset(1)
    
    j = Len(c2.Offset(0, d1_ID_ofs).Value) '最後のIDの文字数
    k = Len(ID_base_str) + 1
    Do While c1.Row < c2.Row
        With c1.Offset(0, d1_ID_ofs)
            ss = .Value
            i = Len(ss)
            If i = j Then
                Exit Do
            End If
            .Value = ID_base_str & String(j - i, "0") & Mid(ss, k)
        End With
        Set c1 = c1.Offset(1)
    Loop
    Set c1 = Nothing
    
    '------------------------------
    ' 後処理
    '------------------------------
    Set last_output_row = c2
    
    Set c2 = Nothing
    
Exit_Proc:
    Erase strMsg
    
    parse_FLTLOG = intCount
End Function


Private Function parse_T700_BladeFLTLOG( _
        ByRef last_output_row As Range, _
        input_fh As Variant, _
        ByVal strNameData As String, _
        Optional ByVal ID_base_str As String = "") As Long

  parse_T700_BladeFLTLOG = -1

  Const FLT_FileName As String = "blade_FLT_"
  Const DateTimeFormat As String = "YYMMDDhhmmss"
  Const CheckPointA As String = "FJ-CDEC"
  Const CheckPointB As String = "MBCONT"
  Dim rngOut As Range
  Dim intCount As Long  ' 有効データ数カウンタ
  Dim log_text As String
  Dim strBuf() As String
  Dim ID_str As String
  Dim data1_str As String
  Dim data2_str As String
  Dim numResult As Long
  Dim strStatus As String
  Dim strLogFileName As String
  Dim strLogDate As String
  Dim date_lower As Variant
  Dim date_upper As Variant
  Dim date_record As Date
  Dim date_val As Variant
  Dim time_val As Variant
  Dim intStep As Long
  Dim strMsg() As String
  Dim cntMsg As Long
  Dim SumMsg As String


  ReDim strMsg(0)
  cntMsg = 0

  ' 抽出範囲指定取り出し
  Call date_get(date_lower, date_upper)

  intCount = 0

  strStatus = input_fh.SourceName & " 読み込み中..."
  If Len(strStatus) > 50 Then
    strStatus = "〜" & Right(strStatus, 48)
  End If
  Application.StatusBar = strStatus

  '----------------------------------------------
  ' ファイル名に付与されている日時情報を切り出す
  '----------------------------------------------
  strLogFileName = Mid(input_fh.SourceName, InStrRev(input_fh.SourceName, "\") + 1)
  strLogDate = Mid(strLogFileName, Len(FLT_FileName) + 1, Len(DateTimeFormat))
  Debug.Print "(parse_T700_BladeFLTLOG):strLogFileName = " & strLogFileName & _
              " strLogDate = " & strLogDate

  If (ParseLogDateFromFileName(strLogDate, date_record) = False) Then
    strMsg(0) = "日時形式不正"
    cntMsg = cntMsg + 1
  Else
    date_val = DateValue(date_record)
    time_val = TimeValue(date_record)
    Debug.Print "(parse_T700_BladeFLTLOG):date_val = " & date_val & _
                " time_val = " & time_val
    ' 範囲外の日付の場合、処理しない
    If date_check(date_record, date_lower, date_upper) = False Then
      GoTo Exit_Proc
    End If
  End If

  '------------------------
  ' FLT情報開始位置(1)検出
  '------------------------
  intStep = 0
  Set rngOut = last_output_row

  Do While input_fh.record_read()
    log_text = input_fh.LineText
    ' 対象行が空白のみで無い場合、処理する
    If Not IsSpaceString(log_text) Then
      ' 最初の行に"FJ-CDEC"があった場合、ループを抜ける
      If (Left(log_text, Len(CheckPointA)) = CheckPointA) Then
        intStep = 1
      ' "FJ-CDEC"が見つからなかった場合、エラーメッセージ追加
      Else
        ReDim Preserve strMsg(cntMsg)
        strMsg(cntMsg) = "「" & CheckPointA & "」行無し"
        cntMsg = cntMsg + 1
      End If

      Exit Do
    End If
  Loop

  If (intStep = 1) Then
    ' レジスタ情報解析
    Do While input_fh.record_read()
      log_text = input_fh.LineText
      ' 対象行が空白のみで無い場合、処理する
      If Not IsSpaceString(log_text) Then
        '------------------------
        ' FLT情報開始位置(2)検出
        '------------------------
        ' "MBCONT"があった場合、ループを抜ける
        If (Left(log_text, Len(CheckPointB)) = CheckPointB) Then
          intStep = 2
          Exit Do
        End If

        log_text = MySpaceCharsReplace(log_text, " ")
        log_text = Trim(log_text)

        ' アドレス検出
        strBuf = Split(log_text, " ")
        If UBound(strBuf) < 1 Then
          ' 空白区切りで無かった場合、該当行はスキップ
          GoTo FJ_CDEC_CONTINUE
        End If

        ' 先頭の"0x"を取り除き、アルファベットは全て大文字に変換
        data1_str = UCase(Mid(strBuf(0), Len("0x") + 1))
        ' 先頭の文字列を"0"→"8"に入れ替え
        data1_str = "8" & Right(data1_str, 7)
        ' 先頭の"0x"を取り除き、アルファベットは全て大文字に変換
        data2_str = UCase(Mid(strBuf(2), Len("0x") + 1))

        ' レジスタ値がAll 0以外の場合
        If (data2_str <> "00000000") Then
          ' 出力シートに出力
          Set rngOut = rngOut.Offset(1)
          intCount = intCount + 1
          ID_str = ID_base_str & input_fh.LineNo
          SumMsg = IIf(cntMsg > 0, Join(strMsg, vbCrLf), "")
          Call parse_result_set(rngOut, intCount, ID_str, date_val, time_val, _
                                data1_str, data2_str, NG_msg:=SumMsg)
        End If

      End If
FJ_CDEC_CONTINUE:
    Loop
  End If

  Select Case (intStep)
  ' "FJ-CDEC"が見つからなかった場合
  Case 0
    ' 出力シートにエラー出力
    Set rngOut = rngOut.Offset(1)
    intCount = intCount + 1
    ID_str = ID_base_str & input_fh.LineNo
    SumMsg = IIf(cntMsg > 0, Join(strMsg, vbCrLf), "")
    Call parse_result_set(rngOut, intCount, ID_str, date_val, time_val, _
                          "", "", NG_msg:=SumMsg)
  ' "MBCONT"が見つからなかった場合
  Case 1
    ReDim Preserve strMsg(cntMsg)
    strMsg(cntMsg) = "「" & CheckPointB & "」行無し"
    cntMsg = cntMsg + 1

    ' 出力シートにエラー出力
    Set rngOut = rngOut.Offset(1)
    intCount = intCount + 1
    ID_str = ID_base_str & input_fh.LineNo
    SumMsg = IIf(cntMsg > 0, Join(strMsg, vbCrLf), "")
    Call parse_result_set(rngOut, intCount, ID_str, date_val, time_val, _
                          "", "", NG_msg:=SumMsg)
  ' 正常ルート("FJ-CDEC","MBCONT"共に有り)
  Case 2
    ' レジスタ情報解析
    Do While input_fh.record_read()
      log_text = input_fh.LineText
      ' 対象行が空白のみで無い場合、処理する
      If Not IsSpaceString(log_text) Then
        log_text = MySpaceCharsReplace(log_text, " ")
        log_text = Trim(log_text)

        ' アドレス検出
        strBuf = Split(log_text, " ")
        If UBound(strBuf) < 1 Then
          ' 空白区切りで無かった場合、該当行はスキップ
          GoTo MBCONT_CONTINUE
        End If

        ' 先頭の"0x"を取り除き、アルファベットは全て大文字に変換
        data1_str = UCase(Mid(strBuf(0), Len("0x") + 1))
        ' 先頭の文字列を"0"→"9"に入れ替え
        data1_str = "9" & Right(data1_str, 7)
        ' 先頭の"0x"を取り除き、アルファベットは全て大文字に変換
        data2_str = UCase(Mid(strBuf(2), Len("0x") + 1))

        ' レジスタ値がAll 0以外の場合
        If (data2_str <> "00000000") Then
          ' 出力シートに出力
          Set rngOut = rngOut.Offset(1)
          intCount = intCount + 1
          ID_str = ID_base_str & input_fh.LineNo
          SumMsg = IIf(cntMsg > 0, Join(strMsg, vbCrLf), "")
          Call parse_result_set(rngOut, intCount, ID_str, date_val, time_val, _
                                data1_str, data2_str, NG_msg:=SumMsg)
        End If

      End If
MBCONT_CONTINUE:
    Loop
  End Select

  Erase strBuf

  '---------------
  ' IDの桁数調整
  '---------------
  Dim LastIDLen As Long
  Dim CurrIdLen As Long
  Dim TempIdLen As Long
  Dim tmpId As String
  Dim rngID As Range
  Set rngID = last_output_row.Offset(1)

  ' 最後のIDの文字数
  LastIDLen = Len(rngOut.Offset(0, d1_ID_ofs).Value)
  CurrIdLen = Len(ID_base_str) + 1

  Do While rngID.Row < rngOut.Row
    With rngID.Offset(0, d1_ID_ofs)
      tmpId = .Value
      TempIdLen = Len(tmpId)
      If TempIdLen = LastIDLen Then
        Exit Do
      End If
      .Value = ID_base_str & _
               String(LastIDLen - TempIdLen, "0") & Mid(tmpId, CurrIdLen)
    End With

    Set rngID = rngID.Offset(1)
  Loop

  Set rngID = Nothing

  '---------
  ' 後処理
  '---------
  Set last_output_row = rngOut
  Set rngOut = Nothing

Exit_Proc:
  Erase strMsg

  parse_T700_BladeFLTLOG = intCount

End Function


Private Function parse_T700_PiuFLTLOG( _
        ByRef last_output_row As Range, _
        input_fh As Variant, _
        ByVal strNameData As String, _
        Optional ByVal ID_base_str As String = "") As Long

  parse_T700_PiuFLTLOG = -1

  Const FLT_FileName As String = "PIU*_FLT_"  ' *:1〜2
  Const DateTimeFormat As String = "YYMMDDhhmmss"
  Const CheckPointA As String = "DCO"
  Const CheckPointB As String = "MDEC"
  Dim rngOut As Range
  Dim intCount As Long  ' 有効データ数カウンタ
  Dim log_text As String
  Dim strBuf() As String
  Dim ID_str As String
  Dim data1_str As String
  Dim data2_str As String
  Dim numResult As Long
  Dim strStatus As String
  Dim strLogFileName As String
  Dim strLogDate As String
  Dim date_lower As Variant
  Dim date_upper As Variant
  Dim date_record As Date
  Dim date_val As Variant
  Dim time_val As Variant
  Dim intStep As Long
  Dim strMsg() As String
  Dim cntMsg As Long
  Dim SumMsg As String
  Dim strZero As String


  ReDim strMsg(0)
  cntMsg = 0

  ' 抽出範囲指定取り出し
  Call date_get(date_lower, date_upper)

  intCount = 0

  strStatus = input_fh.SourceName & " 読み込み中..."
  If Len(strStatus) > 50 Then
    strStatus = "〜" & Right(strStatus, 48)
  End If
  Application.StatusBar = strStatus

  '----------------------------------------------
  ' ファイル名に付与されている日時情報を切り出す
  '----------------------------------------------
  strLogFileName = Mid(input_fh.SourceName, InStrRev(input_fh.SourceName, "\") + 1)
  strLogDate = Mid(strLogFileName, Len(FLT_FileName) + 1, Len(DateTimeFormat))
  Debug.Print "(parse_T700_PiuFLTLOG):strLogFileName = " & strLogFileName & _
              " strLogDate = " & strLogDate

  If (ParseLogDateFromFileName(strLogDate, date_record) = False) Then
    strMsg(0) = "日時形式不正"
    cntMsg = cntMsg + 1
  Else
    date_val = DateValue(date_record)
    time_val = TimeValue(date_record)
    Debug.Print "(parse_T700_PiuFLTLOG):date_val = " & date_val & _
                " time_val = " & time_val
    ' 範囲外の日付の場合、処理しない
    If date_check(date_record, date_lower, date_upper) = False Then
      GoTo Exit_Proc
    End If
  End If

  '------------------------
  ' FLT情報開始位置(1)検出
  '------------------------
  intStep = 0
  Set rngOut = last_output_row

  Do While input_fh.record_read()
    log_text = input_fh.LineText
    ' 対象行が空白のみで無い場合、処理する
    If Not IsSpaceString(log_text) Then
      ' 最初の行に"DCO"があった場合、ループを抜ける
      If (Left(log_text, Len(CheckPointA)) = CheckPointA) Then
        intStep = 1
      ' "DCO"が見つからなかった場合、エラーメッセージ追加
      Else
        ReDim Preserve strMsg(cntMsg)
        strMsg(cntMsg) = "「" & CheckPointA & "」行無し"
        cntMsg = cntMsg + 1
      End If

      Exit Do
    End If
  Loop

  If (intStep = 1) Then
    ' レジスタ情報解析
    Do While input_fh.record_read()
      log_text = input_fh.LineText
      ' 対象行が空白のみで無い場合、処理する
      If Not IsSpaceString(log_text) Then
        '------------------------
        ' FLT情報開始位置(2)検出
        '------------------------
        ' "MDEC"があった場合、ループを抜ける
        If (Left(log_text, Len(CheckPointB)) = CheckPointB) Then
          intStep = 2
          Exit Do
        End If

        log_text = MySpaceCharsReplace(log_text, " ")
        log_text = Trim(log_text)

        ' アドレス検出
        strBuf = Split(log_text, " ")
        If UBound(strBuf) < 1 Then
          ' 空白区切りで無かった場合、該当行はスキップ
          GoTo DCO_CONTINUE
        End If

        ' 先頭の"0x"を取り除き、アルファベットは全て大文字に変換
        data1_str = UCase(Mid(strBuf(0), Len("0x") + 1))
        ' 先頭の"0x"を取り除き、アルファベットは全て大文字に変換後、
        data2_str = UCase(Mid(strBuf(2), Len("0x") + 1))

        ' MDEC行が見つからなかった場合でも処理続行する
        If (Len(data2_str) = 4) Then
          strZero = "0000"
        Else
          strZero = "00000000"
        End If

        ' レジスタ値がAll 0以外の場合
        If (data2_str <> strZero) Then
          ' 出力シートに出力
          Set rngOut = rngOut.Offset(1)
          intCount = intCount + 1
          ID_str = ID_base_str & input_fh.LineNo
          SumMsg = IIf(cntMsg > 0, Join(strMsg, vbCrLf), "")
          Call parse_result_set(rngOut, intCount, ID_str, date_val, time_val, _
                                data1_str, data2_str, NG_msg:=SumMsg)
        End If

      End If
DCO_CONTINUE:
    Loop
  End If

  Select Case (intStep)
  ' "DCO"が見つからなかった場合
  Case 0
    ' 出力シートにエラー出力
    Set rngOut = rngOut.Offset(1)
    intCount = intCount + 1
    ID_str = ID_base_str & input_fh.LineNo
    SumMsg = IIf(cntMsg > 0, Join(strMsg, vbCrLf), "")
    Call parse_result_set(rngOut, intCount, ID_str, date_val, time_val, _
                          "", "", NG_msg:=SumMsg)
  ' "MDEC"が見つからなかった場合
  Case 1
    ReDim Preserve strMsg(cntMsg)
    strMsg(cntMsg) = "「" & CheckPointB & "」行無し"
    cntMsg = cntMsg + 1

    ' 出力シートにエラー出力
    Set rngOut = rngOut.Offset(1)
    intCount = intCount + 1
    ID_str = ID_base_str & input_fh.LineNo
    SumMsg = IIf(cntMsg > 0, Join(strMsg, vbCrLf), "")
    Call parse_result_set(rngOut, intCount, ID_str, date_val, time_val, _
                          "", "", NG_msg:=SumMsg)
  ' 正常ルート("DCO","MDEC"共に有り)
  Case 2
    ' レジスタ情報解析
    Do While input_fh.record_read()
      log_text = input_fh.LineText
      ' 対象行が空白のみで無い場合、処理する
      If Not IsSpaceString(log_text) Then
        log_text = MySpaceCharsReplace(log_text, " ")
        log_text = Trim(log_text)

        ' アドレス検出
        strBuf = Split(log_text, " ")
        If UBound(strBuf) < 1 Then
          ' 空白区切りで無かった場合、該当行はスキップ
          GoTo MDEC_CONTINUE
        End If

        ' 先頭の"0x"を取り除き、アルファベットは全て大文字に変換
        data1_str = UCase(Mid(strBuf(0), Len("0x") + 1))
        ' 先頭の"0x"を取り除き、アルファベットは全て大文字に変換
        data2_str = UCase(Mid(strBuf(2), Len("0x") + 1))

        ' レジスタ値がAll 0以外の場合
        If (data2_str <> "00000000") Then
          ' 出力シートに出力
          Set rngOut = rngOut.Offset(1)
          intCount = intCount + 1
          ID_str = ID_base_str & input_fh.LineNo
          SumMsg = IIf(cntMsg > 0, Join(strMsg, vbCrLf), "")
          Call parse_result_set(rngOut, intCount, ID_str, date_val, time_val, _
                                data1_str, data2_str, NG_msg:=SumMsg)
        End If

      End If
MDEC_CONTINUE:
    Loop
  End Select

  Erase strBuf

  '---------------
  ' IDの桁数調整
  '---------------
  Dim LastIDLen As Long
  Dim CurrIdLen As Long
  Dim TempIdLen As Long
  Dim tmpId As String
  Dim rngID As Range
  Set rngID = last_output_row.Offset(1)

  ' 最後のIDの文字数
  LastIDLen = Len(rngOut.Offset(0, d1_ID_ofs).Value)
  CurrIdLen = Len(ID_base_str) + 1

  Do While rngID.Row < rngOut.Row
    With rngID.Offset(0, d1_ID_ofs)
      tmpId = .Value
      TempIdLen = Len(tmpId)
      If TempIdLen = LastIDLen Then
        Exit Do
      End If
      .Value = ID_base_str & _
               String(LastIDLen - TempIdLen, "0") & Mid(tmpId, CurrIdLen)
    End With

    Set rngID = rngID.Offset(1)
  Loop

  Set rngID = Nothing

  '---------
  ' 後処理
  '---------
  Set last_output_row = rngOut
  Set rngOut = Nothing

Exit_Proc:
  Erase strMsg

  parse_T700_PiuFLTLOG = intCount

End Function


Private Function parse_T700_QsfpFLTLOG( _
        ByRef last_output_row As Range, _
        input_fh As Variant, _
        ByVal strNameData As String, _
        Optional ByVal ID_base_str As String = "") As Long

  parse_T700_QsfpFLTLOG = -1

  Const FLT_FileName As String = "Slot*QSFP*_FLT_"  ' Slot*:1〜2, QSFP*:1〜8
  Const DateTimeFormat As String = "YYYYMMDDThhmmss"
  Const CheckPointA As String = "Flt Log Table"
  Const CheckPointB As String = "Page :"
  Const CheckQSFP As String = "QSFP**"
  Dim rngOut As Range
  Dim intCount As Long  ' 有効データ数カウンタ
  Dim log_text As String
  Dim strBuf() As String
  Dim ID_str As String
  Dim data1_str As String
  Dim data2_str As String
  Dim numResult As Long
  Dim strStatus As String
  Dim strLogFileName As String
  Dim strLogDate As String
  Dim date_lower As Variant
  Dim date_upper As Variant
  Dim date_record As Date
  Dim date_val As Variant
  Dim time_val As Variant
  Dim intStep As Long
  Dim strMsg() As String
  Dim cntMsg As Long
  Dim SumMsg As String
  Dim QSFPType As String


  ReDim strMsg(0)
  cntMsg = 0

  ' 抽出範囲指定取り出し
  Call date_get(date_lower, date_upper)

  intCount = 0

  strStatus = input_fh.SourceName & " 読み込み中..."
  If Len(strStatus) > 50 Then
    strStatus = "〜" & Right(strStatus, 48)
  End If
  Application.StatusBar = strStatus

  '----------------------------------------------
  ' ファイル名に付与されている日時情報を切り出す
  '----------------------------------------------
  strLogFileName = Mid(input_fh.SourceName, InStrRev(input_fh.SourceName, "\") + 1)
  strLogDate = Mid(strLogFileName, Len(FLT_FileName) + 1, Len(DateTimeFormat))
  Debug.Print "(parse_T700_QsfpFLTLOG):strLogFileName = " & strLogFileName & _
              " strLogDate = " & strLogDate

  If (ParseLogDateFromFileNameQSFP(strLogDate, date_record) = False) Then
    strMsg(0) = "日時形式不正"
    cntMsg = cntMsg + 1
  Else
    date_val = DateValue(date_record)
    time_val = TimeValue(date_record)
    Debug.Print "(parse_T700_QsfpFLTLOG):date_val = " & date_val & _
                " time_val = " & time_val
    ' 範囲外の日付の場合、処理しない
    If date_check(date_record, date_lower, date_upper) = False Then
      GoTo Exit_Proc
    End If
  End If

  '------------------------
  ' FLT情報開始位置(1)検出
  '------------------------
  intStep = 0
  Set rngOut = last_output_row

  Do While input_fh.record_read()
    log_text = input_fh.LineText
    ' 対象行が空白のみで無い場合、処理する
    If Not IsSpaceString(log_text) Then
      ' 最初の行に"Flt Log Table"があった場合、QSFP種別を取得＆ループを抜ける
      If (Right(log_text, Len(CheckPointA)) = CheckPointA) Then
        QSFPType = Left(log_text, Len(CheckQSFP))
        Debug.Print "(parse_T700_QsfpFLTLOG):QSFPType = " & QSFPType

        intStep = 1
      ' "Flt Log Table"が見つからなかった場合、エラーメッセージ追加
      Else
        ReDim Preserve strMsg(cntMsg)
        strMsg(cntMsg) = "「" & CheckPointA & "」行無し"
        cntMsg = cntMsg + 1
      End If

      Exit Do
    End If
  Loop

  If (intStep = 1) Then
    Do While input_fh.record_read()
      log_text = input_fh.LineText
      ' 対象行が空白のみで無い場合、処理する
      If Not IsSpaceString(log_text) Then
        ' "Page :"があった場合、ループを抜ける
        If (Left(log_text, Len(CheckPointB)) = CheckPointB) Then
          intStep = 2
          Exit Do
        End If
      End If
    Loop
  End If

  Select Case (intStep)
  ' "Flt Log Table"が見つからなかった場合
  Case 0
    ' 出力シートにエラー出力
    Set rngOut = rngOut.Offset(1)
    intCount = intCount + 1
    ID_str = ID_base_str & input_fh.LineNo
    SumMsg = IIf(cntMsg > 0, Join(strMsg, vbCrLf), "")
    Call parse_result_set(rngOut, intCount, ID_str, date_val, time_val, _
                          "", "", NG_msg:=SumMsg)
  ' "Page :"が見つからなかった場合
  Case 1
    ReDim Preserve strMsg(cntMsg)
    strMsg(cntMsg) = "「" & CheckPointB & "」行無し"
    cntMsg = cntMsg + 1

    ' 出力シートにエラー出力
    Set rngOut = rngOut.Offset(1)
    intCount = intCount + 1
    ID_str = ID_base_str & input_fh.LineNo
    SumMsg = IIf(cntMsg > 0, Join(strMsg, vbCrLf), "")
    Call parse_result_set(rngOut, intCount, ID_str, date_val, time_val, _
                          "", "", NG_msg:=SumMsg)
  ' 正常ルート("Flt Log Table","Page :"共に有り)
  Case 2
    ' レジスタ情報解析
    Do While input_fh.record_read()
      log_text = input_fh.LineText
      ' 対象行が空白のみで無い場合、処理する
      If Not IsSpaceString(log_text) Then
        log_text = MySpaceCharsReplace(log_text, " ")
        log_text = Trim(log_text)
        ' アドレス検出
        strBuf = Split(log_text, " ")
        If UBound(strBuf) < 1 Then
          ' 空白区切りで無かった場合、該当行はスキップ
          GoTo QSFP_CONTINUE
        End If

        ' QSFP種別によって処理分け
        Select Case (QSFPType)
        Case "QSFPDD"
          ' ------------------------------------------
          ' 実際の値(例)
          ' "Page : addr(dec) :addr(hex) : Reg Data"
          ' "00h : 3 :0x03 : 0x06"
          ' ------------------------------------------
          ' Page : addr(dec) :addr(hex)をまとめ、アドレスとして抜き出す
          ' 先頭が"1*h"の場合、全て"11h"に変換する。
          If strBuf(0) Like "1*h" Then
            strBuf(0) = "11h"
          End If

          data1_str = strBuf(0) & " " & strBuf(1) & " " & strBuf(2) & " " & strBuf(3)
          ' 先頭の"0x"を取り除き、アルファベットは全て大文字に変換
          data2_str = "000000" & UCase(Mid(strBuf(5), Len("0x") + 1))

        Case "QSFP28"
          ' ------------------------------------------
          ' 実際の値(例)
          ' "Page : addr(dec) :addr(hex) : Reg Data"
          ' "00h : 3 :0x00000003 : 0x0000000F"
          ' ------------------------------------------
          ' Page : addr(dec) :addr(hex)をまとめ、アドレスとして抜き出す
          data1_str = strBuf(0) & " " & strBuf(1) & " " & strBuf(2) & " " & strBuf(3)
          ' 先頭の"0x"を取り除き、アルファベットは全て大文字に変換
          data2_str = UCase(Mid(strBuf(5), Len("0x") + 1))

        End Select

        ' レジスタ値がAll 0以外の場合
        If (data2_str <> "00000000") Then
          ' 出力シートに出力
          Set rngOut = rngOut.Offset(1)
          intCount = intCount + 1
          ID_str = ID_base_str & input_fh.LineNo
          SumMsg = IIf(cntMsg > 0, Join(strMsg, vbCrLf), "")
          Call parse_result_set(rngOut, intCount, ID_str, date_val, time_val, _
                                data1_str, data2_str, NG_msg:=SumMsg)
        End If

      End If
QSFP_CONTINUE:
    Loop
  End Select

  Erase strBuf

  '---------------
  ' IDの桁数調整
  '---------------
  Dim LastIDLen As Long
  Dim CurrIdLen As Long
  Dim TempIdLen As Long
  Dim tmpId As String
  Dim rngID As Range
  Set rngID = last_output_row.Offset(1)

  ' 最後のIDの文字数
  LastIDLen = Len(rngOut.Offset(0, d1_ID_ofs).Value)
  CurrIdLen = Len(ID_base_str) + 1

  Do While rngID.Row < rngOut.Row
    With rngID.Offset(0, d1_ID_ofs)
      tmpId = .Value
      TempIdLen = Len(tmpId)
      If TempIdLen = LastIDLen Then
        Exit Do
      End If
      .Value = ID_base_str & _
               String(LastIDLen - TempIdLen, "0") & Mid(tmpId, CurrIdLen)
    End With

    Set rngID = rngID.Offset(1)
  Loop

  Set rngID = Nothing

  '---------
  ' 後処理
  '---------
  Set last_output_row = rngOut
  Set rngOut = Nothing

Exit_Proc:
  Erase strMsg

  parse_T700_QsfpFLTLOG = intCount

End Function


Private Sub parse_result_set( _
        out_base As Range, _
        ByVal intCount As Long, _
        ByVal ID_str As String, _
        date_val As Variant, _
        time_val As Variant, _
        ByVal data1_str As String, _
        ByVal data2_str As String, _
        Optional other_info As String = "", _
        Optional NG_msg As String = "" _
    )
    
    Call set_result(out_base.Offset(0, 0), intCount)
    Call set_result(out_base.Offset(0, d1_ID_ofs), ID_str)
    
    If IsNull(date_val) Then
        Call set_result(out_base.Offset(0, d1_Date_ofs), "")
        Call set_result(out_base.Offset(0, d1_Time_ofs), "")
    Else
        Call set_result(out_base.Offset(0, d1_Date_ofs), date_val, strForm:="yyyy/mm/dd")
        Call set_result(out_base.Offset(0, d1_Time_ofs), time_val, strForm:="hh:mm:ss")
    End If
    
    Call set_result(out_base.Offset(0, d1_data1_ofs), data1_str)
    Call set_result(out_base.Offset(0, d1_data2_ofs), data2_str)
    
    Call set_result(out_base.Offset(0, d1_Info_ofs), other_info, bWrapText:=False)
    Call set_result(out_base.Offset(0, d1_NG_ofs), NG_msg, bWrapText:=False)
End Sub

'タイムスタンプ切り出し
Private Function parse_LogDate( _
        ByVal strRecord As String, _
        ByRef dtDate As Date, _
        Optional ByRef dtUTC As Variant) As Boolean
    
    parse_LogDate = False
    
    Dim strTmp As String
    Dim strDate As String
    Dim strTime As String
    Dim strDiff As String 'UTCとの差分
    Dim dtDiff As Date
    
    Dim i As Long
    Dim j As Long
    
    strTmp = Trim(strRecord)
    
    ' 形式チェック
    If strTmp Like "####-##-##T##:##:##*" Then
                  ' yyyy-mm-ddThh:nn:ss
        i = InStr(strTmp, " ")
        If i > 0 Then
            strTmp = Left(strTmp, i - 1)
        End If
        
        i = InStr(strTmp, "T")
        strDate = Left(strTmp, i - 1)
        strTime = Mid(strTmp, i + 1, 8)
        strTmp = Mid(strTmp, i + 9)
        
        ' UTCとの差分
        If strTmp Like "[+-]##:##" Then
            strDiff = strTmp
        Else
            strDiff = ""
        End If
        
    ElseIf strTmp Like "[A-Z][a-z][a-z] [A-Z][a-z][a-z] *# ##:##:## ####" Then
                      ' ddd             mmm             d  hh:nn:ss yyyy
        j = InStrRev(strTmp, " ")
        i = InStrRev(strTmp, ":", j - 1)
        i = InStrRev(strTmp, ":", i - 1)
        i = InStrRev(strTmp, " ", i - 1)
        
        strTime = Mid(strTmp, i + 1, j - i)
        strDate = Trim(Left(strTmp, i - 1)) & Mid(strTmp, j)
        
        i = InStr(strDate, " ")
        strDate = Mid(strDate, i + 1) '曜日を除去
        strDiff = ""
    Else
        Exit Function
    End If
    
    dtDate = CDate(strDate & " " & strTime)
    
    If IsMissing(dtUTC) Then
        
    ElseIf strDiff = "" Then
        dtUTC = dtDate
    Else
        dtDiff = TimeValue(Mid(strDiff, 2))
        If Left(strDiff, 1) = "+" Then
            dtUTC = dtDate + dtDiff
        Else
            dtUTC = dtDate - dtDiff
        End If
    End If
    
    parse_LogDate = True
End Function


Private Function ParseLogDateFromFileName( _
    ByVal strRecord As String, _
    ByRef dtDate As Date) As Boolean

  ParseLogDateFromFileName = False

  Const TransFormat As String = "####/##/## ##:##:##"
  Dim strDate As String

  ' 入力される日時文字の例：200704103350
  Debug.Print "(ParseLogDateFromFileName):strRecord = " & strRecord

  strDate = Format("20" & strRecord, TransFormat)
  Debug.Print "(ParseLogDateFromFileName):strDate = " & strDate

  ' 文字列→日付への変換前チェック
  If (IsDate(strDate) = False) Then
    Exit Function
  End If

  ' 文字列→日付に変換
  dtDate = CDate(strDate)

  ParseLogDateFromFileName = True

End Function


Private Function ParseLogDateFromFileNameQSFP( _
    ByVal strRecord As String, _
    ByRef dtDate As Date) As Boolean

  ParseLogDateFromFileNameQSFP = False

  Const TransFormat As String = "####/##/## ##:##:##"
  Dim strDate As String

  ' 入力される日時文字の例：20200704T102809
  Debug.Print "(ParseLogDateFromFileNameQSFP):strRecord = " & strRecord

  strDate = Format(Replace(strRecord, "T", ""), TransFormat)
  Debug.Print "(ParseLogDateFromFileNameQSFP):strDate = " & strDate

  ' 文字列→日付への変換前チェック
  If (IsDate(strDate) = False) Then
    Exit Function
  End If

  ' 文字列→日付に変換
  dtDate = CDate(strDate)

  ParseLogDateFromFileNameQSFP = True

End Function


Private Function date_get( _
        ByRef date_lower As Variant, _
        ByRef date_upper As Variant) As Boolean
    
    date_get = False
    
    Dim date_str As String  '日付指定
    Dim date_num As String    '日付範囲指定
    Dim date_range As String    '日付範囲単位
    Dim time_str    As String   '時刻指定
    Dim time_range  As String    '時刻範囲指定
    Dim ss  As String
    
    ''日付指定取得
    date_str = toolPara_get("TargetDate") '日付指定
    If Not IsDate(date_str) Then    '日付指定がない場合
        date_upper = Null
        date_lower = Null
        Exit Function
    End If
    
    date_upper = CDate(date_str)
    date_lower = date_upper
    
    date_num = toolPara_get("TargetDateRange") '日付範囲指定
    If date_num = "" Then
    ElseIf IsNumeric(date_num) Then  '日付範囲指定がある場合
        
        date_range = toolPara_get("TargetDateUnit") '日付範囲単位
        Select Case date_range
        Case "日前後"
            date_lower = DateAdd("d", -1 * date_num, date_upper)
            date_upper = DateAdd("d", date_num, date_upper)
        Case "日前まで"
            date_lower = DateAdd("d", -1 * date_num, date_upper)
        Case "週前まで"
            date_lower = DateAdd("ww", -1 * date_num, date_upper)
        Case "月前まで"
            date_lower = DateAdd("m", -1 * date_num, date_upper)
        Case "年前まで"
            date_lower = DateAdd("yyyy", -1 * date_num, date_upper)
        End Select
    End If
    
    ''時刻指定取得
    If date_lower <> date_upper Then    '複数日指定の場合は時刻指定を無視する
        time_str = ""
    Else
        time_str = toolPara_get("TargetTime") '時刻指定
        If time_str = "" Then
        ElseIf Not IsDate(time_str) Then    '不正フォーマット
            time_str = ""
        End If
    End If
    
    If time_str <> "" Then  '時刻指定がある場合
        date_upper = date_upper + TimeValue(time_str)
        date_lower = date_upper
        
        time_range = toolPara_get("TargetTimeRange") '時刻範囲指定
        If time_range Like "*秒前後" Then
            ss = Left(time_range, Len(time_range) - 3)  '秒数取り出し
            date_lower = DateAdd("s", 0 - CLng(ss), date_lower)
            date_upper = DateAdd("s", CLng(ss), date_upper)
        ElseIf time_range Like "*秒前まで" Then
            ss = Left(time_range, Len(time_range) - 4)  '秒数取り出し
            date_lower = DateAdd("s", 0 - CLng(ss), date_lower)
        End If
    Else
        date_lower = date_lower + TimeValue("00:00:00")
        date_upper = date_upper + TimeValue("23:59:59")
    End If
    
    date_get = True
End Function


Private Function date_check(rec_date As Variant, date1 As Variant, date2 As Variant) As Boolean
    date_check = False
    
    If Not IsNull(rec_date) Then
        If Not IsNull(date1) Then
            If rec_date < date1 Then
                Exit Function
            End If
        End If
        
        If Not IsNull(date2) Then
            If rec_date > date2 Then
                Exit Function
            End If
        End If
    End If
    
    date_check = True
End Function
