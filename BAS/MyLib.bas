Attribute VB_Name = "MyLib"
'-------------------------------------------------------------
' All Rights Reserved, Copyright (C) FUJITSU LIMITED 2017
'
'   汎用ルーチン
'
'   更新日: 2017年 3月 9日
'
'-------------------------------------------------------------
Option Private Module
Option Explicit

Public Function MyFullPathGet(ByVal strPath As String) As String
    Dim objFSO As Object
    
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    If objFSO.GetDriveName(strPath) = "" Then   '相対パスの場合
        MyFullPathGet = objFSO.GetAbsolutePathName(ThisWorkbook.Path & "\" & strPath)
            '注意）GetAbsolutePathNameは、指定したファイル名と実在するファイル名に
            '      大文字小文字の違いがある場合、実在するファイル名を返す。
    Else
        MyFullPathGet = strPath
    End If
    Set objFSO = Nothing
    
End Function

Public Function MyFileNameGet(ByVal strPath As String) As String
    Dim objFSO  As Object
    
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    MyFileNameGet = objFSO.GetFileName(strPath)
    Set objFSO = Nothing

End Function

Public Function MyFolderNameGet(ByVal strPath As String) As String
    Dim objFSO  As Object
    
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    MyFolderNameGet = objFSO.GetParentFolderName(strPath)
    Set objFSO = Nothing

End Function

Public Function MyFileDateGet( _
        ByVal strFullPath As String, _
        ByRef dtLastModified As Date) As Boolean
    Dim objFSO  As Object
    
    On Error Resume Next
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    dtLastModified = objFSO.GetFile(strFullPath).DateLastModified
    If Err.Number = 0 Then
        MyFileDateGet = True
    Else
        MyFileDateGet = False
    End If
    Set objFSO = Nothing
    
End Function

' 注意)
'・Dir(フォルダ名)は、存在していても偽を返す。
'・Dir(〜,vbDirectory)は、ファイルでも真を返す。
'・Dir と GetAttr は、パス名が不正文字を含むとエラーになる
'・GetAttrは、存在しないパス名でエラーになる

Public Function IsExistPath(ByVal strFullPath As String) As Boolean
    IsExistPath = False
    
    On Error GoTo Error_Exit
    If GetAttr(strFullPath) Then
        IsExistPath = True
    End If
    Exit Function
    
Error_Exit:
End Function

Public Function IsFolderName(ByVal strFullPath As String) As Boolean
    IsFolderName = False
    
    If IsExistPath(strFullPath) = False Then
        Exit Function
    ElseIf GetAttr(strFullPath) And vbDirectory Then
        IsFolderName = True
    End If
    
End Function

Public Function IsEnablePath( _
        strFullPath As String, _
        Optional ByVal Writable As Boolean = False) As Boolean
        
    IsEnablePath = False
    
    If IsExistPath(strFullPath) = False Then
        Exit Function
    ElseIf GetAttr(strFullPath) And (vbHidden + vbSystem) Then
        Exit Function
    ElseIf Writable Then
        If GetAttr(strFullPath) And vbReadOnly Then
            Exit Function
        End If
    End If

    IsEnablePath = True
End Function

Sub MyRangeSelect(objRange As Range)
    '注）WorkSheetがActiveでないとCellのSelectが実行エラーになる
    Call MySheetActivate(objRange.Worksheet)
    objRange.Select
End Sub

Sub MySheetActivate(wsTarget As Worksheet)
    '注）BookがActiveでないとWorkSheetのActivateが実行エラーになる
    wsTarget.Parent.Activate
    wsTarget.Select
End Sub

Sub MyScreenUpdate()
    Dim booSave As Variant
    booSave = Application.ScreenUpdating
    Application.ScreenUpdating = True
    Application.ScreenUpdating = booSave
End Sub

' テーブル取得
Public _
Function GetTableByName( _
        ByVal テーブル名 As String, _
        wsTarget As Worksheet, _
        Optional ByVal NoMsg As Boolean = False) As ListObject
        
    Dim objTmp As ListObject
    Dim objFound As ListObject
    
    Set objFound = Nothing
    For Each objTmp In wsTarget.ListObjects
        If objTmp.Name = テーブル名 Then
            Set objFound = objTmp
            Exit For
        End If
    Next objTmp
    
    If (objFound Is Nothing) And (NoMsg = False) Then
        Call MsgBox(wsTarget.Name & "シートに「" & テーブル名 & "」というテーブルがありません。" _
            , vbCritical)
    End If
    
    Set GetTableByName = objFound
End Function

'=============================================================
'
'    ワークブックの処理
'
'=============================================================
Public _
Function MyBookNameGet(ByVal strPath As String) As String
    Dim strTmp As String
    
    strTmp = MyFileNameGet(strPath)
    
    strTmp = Replace(strTmp, "[", "(")
    strTmp = Replace(strTmp, "]", ")")
    '注）ブック名(WorkBook.Name)の "["または"]"は、Excel 内では "(",")" に変換されている。
    '     WorkBook.Path 及び、WorkBook.FullName では、変換されない。
    
    MyBookNameGet = strTmp
End Function

Public _
Function MyRefBookOpen(ByVal strPath As String) As Workbook
        
    Dim wb As Workbook
    
    Set MyRefBookOpen = Nothing
    
    If MyOpenBookCheck(wb, strPath, ReuseEnable:=True) = False Then
        Exit Function
    ElseIf wb Is Nothing Then
        Set wb = MyBookOpen(strPath, ReadOnly:=True)
    End If
    
    Set MyRefBookOpen = wb
End Function
    
' 指定されたブックがオープンされていれば、取得。
'   ReuseEnable : 取得できた場合にエラーメッセージを表示しない。
'   ReadOnly    : 取得したブックの ReadOnly属性と一致しない場合、エラーメッセージを表示。
Public _
Function MyOpenBookCheck( _
            ByRef wbResult As Workbook, _
            ByVal strPath As String, _
            ByVal ReuseEnable As Boolean, _
            Optional ReadOnly As Variant _
        ) As Boolean
    
    Dim strFullPath As String
    Dim strBookName As String
    Dim objBook As Workbook
    Dim strMsg As String
    
    MyOpenBookCheck = False
    Set wbResult = Nothing
    
    strBookName = MyBookNameGet(strPath)
    
    '既にオープンされているか調べる
    For Each objBook In Workbooks
        If UCase(objBook.Name) = UCase(strBookName) Then
            strFullPath = MyFullPathGet(strPath)
            
            If objBook.FullName <> strFullPath Then    '注）同名の別ファイルかもしれないので完全パスで比較する。
                strMsg = "同じブック名の別ファイル" & vbCrLf _
                    & objBook.FullName & vbCrLf & vbCrLf _
                    & "が開いているため、 & vbCrLf" _
                    & strFullPath & vbCrLf _
                    & "が開けません" & vbCrLf & vbCrLf _
                    & "クローズして再実行して下さい"
                    
            ElseIf ReuseEnable = False Then
                strMsg = strFullPath & vbCrLf & vbCrLf _
                        & "がオープンされています。" & vbCrLf _
                        & "クローズして再実行して下さい"
                        
            ElseIf Not IsMissing(ReadOnly) Then
                If objBook.ReadOnly <> ReadOnly Then
                    strMsg = strFullPath & vbCrLf & vbCrLf _
                            & "がオープンされています。" & vbCrLf _
                            & "クローズして再実行して下さい"
                End If
            End If
            
            If strMsg <> "" Then
                Application.ScreenUpdating = True
                objBook.Activate
                Set objBook = Nothing
                
                Call MsgBox(strMsg, vbCritical)
                
                Exit Function 'ＮＧ
            End If
            
            Set wbResult = objBook
            Exit For ' ＯＫ
        End If
    Next objBook

    MyOpenBookCheck = True
    
End Function

Public _
Function MyBookOpen( _
            ByVal strPath As String, _
            ByVal ReadOnly As Boolean, _
            Optional ByVal UpdateLinks As Boolean = False _
        ) As Workbook
    
    Dim strFullPath As String
    Dim strBookName As String
    Dim objBook As Workbook
    
    Dim booSave As Boolean
    
    Set MyBookOpen = Nothing
        
    strFullPath = MyFullPathGet(strPath)
    strBookName = MyBookNameGet(strPath)
    
    On Error Resume Next
    booSave = Application.ScreenUpdating
    Application.ScreenUpdating = False
    Set objBook = Workbooks.Open(strFullPath, ReadOnly:=ReadOnly, UpdateLinks:=UpdateLinks)
    Application.ScreenUpdating = booSave
    If Err.Number <> 0 Then
        Call MsgBox("'" & strFullPath & "'" & vbCrLf _
            & "がオープンできません" & vbCrLf & vbCrLf _
            & Err.Description, vbExclamation)
            
        Err.Clear
        Exit Function
    End If
    On Error GoTo 0
        
    If objBook.FullName <> strFullPath Then    '注）Excelのバグ(？)対処
        Call MsgBox("'" & strFullPath & "'" & vbCrLf _
            & "の OpenでExcelに異常が発生しました。違うファイル" & vbCrLf _
            & objBook.FullName & vbCrLf _
            & "がOpenされました。" _
            , vbExclamation)
            
        Set objBook = Nothing
        End
    End If

    Set MyBookOpen = objBook
    
End Function

'=============================================================
'
'    ワークシートの処理
'
'=============================================================

'--------------------------------------------------------------
' シート名でワークシートを取り出す
'--------------------------------------------------------------
Public _
Function GetWorkSheetByName( _
            ByVal SearchName As String, _
            ByVal TargetBook As Workbook, _
            Optional ByVal NoMsg As Boolean = False _
        ) As Worksheet
    
    Dim objWS   As Worksheet
    For Each objWS In TargetBook.Worksheets
        If objWS.Name = SearchName Then
            Set GetWorkSheetByName = objWS
            Exit Function
        End If
    Next objWS
    
    If NoMsg = False Then
        Call MsgBox(TargetBook.Name & vbCrLf _
            & "に""" & SearchName & """シートがありません。", vbCritical)
    End If
    
    Set GetWorkSheetByName = Nothing
End Function

'--------------------------------------------------------------
' 保護されたシートをマクロから操作可能にする。
'--------------------------------------------------------------
Public Sub MyProtectReset(ws As Worksheet)
    Dim mySave1  As Protection
    Dim myflag1 As Boolean
    Dim myflag2 As Boolean
    Dim myflag3 As Boolean
    
    myflag1 = ws.ProtectDrawingObjects
    myflag2 = ws.ProtectContents
    myflag3 = ws.ProtectScenarios
    If (myflag1 Or myflag2 Or myflag3) And (ws.ProtectionMode = False) Then
        Set mySave1 = ws.Protection
        ws.Unprotect
        
        With mySave1
            ws.Protect UserInterfaceOnly:=True _
                , DrawingObjects:=myflag1, Contents:=myflag2, Scenarios:=myflag3 _
                , AllowDeletingColumns:=.AllowDeletingColumns _
                , AllowDeletingRows:=.AllowDeletingRows _
                , AllowFiltering:=.AllowFiltering _
                , AllowFormattingCells:=.AllowFormattingCells _
                , AllowFormattingColumns:=.AllowFormattingColumns _
                , AllowFormattingRows:=.AllowFormattingRows _
                , AllowInsertingColumns:=.AllowInsertingColumns _
                , AllowInsertingHyperlinks:=.AllowInsertingHyperlinks _
                , AllowInsertingRows:=.AllowInsertingRows _
                , AllowSorting:=.AllowSorting _
                , AllowUsingPivotTables:=.AllowUsingPivotTables
        End With
        Set mySave1 = Nothing
    End If
End Sub

'=============================================================
'
'    検索処理
'
'=============================================================
'--------------------------------------------------------------
' 指定列の最終データの行番号を返す ※最初の空白値まで
'--------------------------------------------------------------
Function MyDataEndRowGet(StartCell As Range) As Variant

    Dim wsTarget  As Worksheet
    Dim intTargetColumn As Variant
    
    Dim intMinRow   As Variant
    Dim intMaxRow   As Variant
    Dim intFound    As Variant
    Dim i As Variant
    
    With StartCell
        Set wsTarget = .Worksheet
        intTargetColumn = .Column
        intMinRow = .Row
    End With
    
    With wsTarget.UsedRange
        intMaxRow = .Cells(.Cells.Count).Row
    End With
        
    intFound = intMaxRow
    For i = intMinRow To intMaxRow
        If IsSpaceString(wsTarget.Cells(i, intTargetColumn).Value) Then
            If i = intMinRow Then
                intFound = 0
            Else
                intFound = i - 1
            End If
            Exit For
        End If
    Next i

    MyDataEndRowGet = intFound
End Function

'--------------------------------------------------------------
' 指定列の最終データの行番号を返す ※最後の非空白値まで
'--------------------------------------------------------------
Function MyUsedMaxRowGet(TargetCell As Range) As Variant

    Dim wsTarget  As Worksheet
    Dim intTargetColumn As Variant
    
    Dim intMinRow   As Variant
    Dim intMaxRow   As Variant
    Dim intFound    As Variant
    Dim i As Variant
    
    With TargetCell
        Set wsTarget = .Worksheet
        intTargetColumn = .Column
        intMinRow = .Row
    End With
    
    With wsTarget.UsedRange
        intMaxRow = .Cells(.Cells.Count).Row
    End With
    
    intFound = 0
    For i = intMaxRow To intMinRow Step -1
        If Not IsSpaceString(wsTarget.Cells(i, intTargetColumn).Value) Then
            intFound = i
            Exit For
        End If
    Next i

    MyUsedMaxRowGet = intFound
End Function

'ワークシートで使用されている最終行を取得
Public Function GetRealUsedRangeEndRow(rngTop As Range) As Range
    Dim c1 As Range
    Dim c2 As Range
    Dim intRow As Variant
    
    intRow = rngTop.Row
    With rngTop.Worksheet.UsedRange
        Set c1 = .Rows(.Rows.Count)
    End With
    Do While c1.Row > intRow
        For Each c2 In c1.Cells
            If Not IsEmpty(c2.Value) Then
                intRow = c2.Row
                Exit Do
            End If
        Next c2
        Set c1 = c1.Offset(-1)
    Loop
    
    GetRealUsedRangeEndRow = intRow
End Function

'--------------------------------------------------------------
' ワークシートの最終データのセルを返す
'--------------------------------------------------------------
Public Function get_lastDataCell(ws As Worksheet) As Range
    With ws.UsedRange
        Set get_lastDataCell = .Cells(.Rows.Count, .Columns.Count)
    End With
End Function

'指定列で使用されている最後のセルを取得
Private Function GetUsedColumnEndCell(ByVal rngTop As Range) As Range
    Dim rngEnd As Range
    Dim ws As Worksheet
    
    If rngTop.Columns.Count > 1 Then
        Call MsgBox("Illegal Use", vbCritical)
        Stop
    End If
    
    Set ws = rngTop.Worksheet
    With ws.UsedRange
        Set rngEnd = ws.Cells(.Row + .Rows.Count - 1, rngTop.Column)
    End With
    Do While IsEmpty(rngEnd.Value)
        If rngEnd.Row <= rngTop.Row Then
            Set rngEnd = rngTop
            Exit Do
        End If
        Set rngEnd = rngEnd.Offset(-1)
    Loop
    
    Set GetUsedColumnEndCell = rngEnd
End Function

'--------------------------------------------------------------
' データシート検索
'--------------------------------------------------------------
Public Function row_search(start_cell As Range, search_val As Variant) As Range
    Dim endRow  As Long
    Dim c1  As Range
    
    endRow = get_lastDataCell(start_cell.Worksheet).Row
    
    On Error Resume Next
    
    Set row_search = Nothing
    Set c1 = start_cell.Cells(1, 1)
    Do While c1.Row <= endRow
        If Trim$(c1.Value) = search_val Then
            Set row_search = c1
            Exit Do
        End If
        
        Set c1 = c1.Offset(1)
        If Err.Number <> 0 Then
            Err.Clear
            Exit Do
        End If
    Loop
    Set c1 = Nothing
    
    On Error GoTo 0
    
End Function

Public Function col_search(start_cell As Range, search_val As Variant) As Range
    Dim endCol  As Long
    Dim c1  As Range
    
    endCol = get_lastDataCell(start_cell.Worksheet).Column
    
    On Error Resume Next
    
    Set col_search = Nothing
    Set c1 = start_cell.Cells(1, 1)
    Do While c1.Column <= endCol
        If Trim$(c1.Value) = search_val Then
            Set col_search = c1
            Exit Do
        End If
        
        Set c1 = c1.Offset(0, 1)
        If Err.Number <> 0 Then
            Err.Clear
            Exit Do
        End If
    Loop
    Set c1 = Nothing
    
    On Error GoTo 0
    
End Function

'=============================================================
'
'    名前定義の処理
'
'=============================================================
Public _
Function 名前の参照範囲取得( _
            ByVal ws As Worksheet, _
            ByVal strName As String, _
            Optional NoMsg As Boolean = False) As Range
    
    If ws Is Nothing Then
        Call MsgBox("Bug!!", vbCritical)
        Stop
    End If
    
    On Error Resume Next
    
    Set 名前の参照範囲取得 = ws.Range(strName)
    If Err.Number <> 0 Then
        If Not NoMsg Then
            Call MsgBox("'" & ws.Name & "'シートに'" & strName & "'の名前定義がありません", vbCritical)
        End If
        Err.Clear
        
        Set 名前の参照範囲取得 = Nothing
    End If
    
    On Error GoTo 0
    
End Function

'=============================================================
'
'    文字列処理
'
'=============================================================
Public _
Function IsSpaceString(ByVal 値 As String) As Boolean
    
    ' 空白文字の除去
    If MySpaceCharsReplace(値, "") = "" Then
        IsSpaceString = True
    Else
        IsSpaceString = False
    End If

End Function

Public _
Function MySpaceCharsReplace( _
        ByVal 変換対象 As String, _
        ByVal 置換文字 As String) As String
    
    Dim strTmp As String
    Dim i As Long
        
    strTmp = 変換対象
    
    ' 空白文字を半角空白に置換
    strTmp = Replace(strTmp, "　", " ") '全角空白
    strTmp = Replace(strTmp, vbTab, " ")
    strTmp = Replace(strTmp, vbLf, " ")
    strTmp = Replace(strTmp, vbCr, " ")
    strTmp = Replace(strTmp, vbBack, " ")
    
    '連続する空白を１文字に変換
    Do
        i = Len(strTmp)
        strTmp = Replace(strTmp, "  ", " ")
    Loop While Len(strTmp) <> i
    
    MySpaceCharsReplace = Replace(strTmp, " ", 置換文字)
    
End Function
