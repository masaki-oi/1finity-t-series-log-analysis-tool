Attribute VB_Name = "Module1"
' All Rights Reserved, Copyright (C) FUJITSU LIMITED 2017
'
'   更新日: 2017年 8月31日
'
Option Explicit

Public verbose_flag    As Boolean

Public Const control_shname = "操作"
Public Const deftab_shname = "参照ファイル定義"

'参照ファイル定義
Private Const strTabName_UnitNoList = "UNIT図番テーブル"

Private Const strDefName_ExtractTool = "解凍ツール"
Private Const strDefName_ExtractInput = "解凍元フォルダ_初期値"
Private Const strDefName_ExtractOutput = "解凍先フォルダ"

Private Const strDefName_FLTLOG = "FLTログ_初期値"
Private Const strDefName_MacroFlag = "マクロ終了フラグ"
Private Const strDefName_SaveFile = "保存先ファイル名"

'解析パラメータの位置
Private Const para1_base = "抽出範囲_日付"
Private Const para2_base = "抽出範囲_時刻"
Private Const para3_base = "UNIT図番指定"
Private Const para4_base = "出力フィルタ"
Private Const para5_base = "Blade名"

'swerr自動実行＆貼り付け用
Public Const sPasteLog_shname = "LogText"

'データ領域の定義
Public Const data_base_line = 19   '出力項目名の行

Public Const d1_data_base_col = "B"    'ログテキストからの抽出データ
Public Const d1_No_ofs = 0     '遠し番号の列
Public Const d1_ID_ofs = 1     '「ID」列
Public Const d1_Date_ofs = 2
Public Const d1_Time_ofs = 3
Public Const d1_data1_ofs = 4  '「Addr」列
Public Const d1_data2_ofs = 5  '「data2」列
Public Const d1_Info_ofs = 6   '「Other Info」列
Public Const d1_NG_ofs = 7     '「解析NG」列
Public Const d1_data_max_ofs = 7  '最終列

Public Const d2_data_base_col = "K"    '検索結果
Public Const d2_No_ofs = 0     '遠し番号の列
Public Const d2_ID_ofs = 1     '「ID」列
Public Const d2_Addr_ofs = 2    '「Addr」列
Public Const d2_Bit_ofs = 3        '「Bit」列
Public Const d2_BitName_ofs = 4    '「通知bit名」列
Public Const d2_SoftVer_ofs = 5    '「soft ver」列 ※Reserve
Public Const d2_FaultInfo_ofs = 6  '「障害内容」列 ※Reserve
Public Const d2_ErrorType_ofs = 7  '「Errorの種類」列  ※Reserve
Public Const d2_RepairPart_ofs = 8     '「被疑部品」列
Public Const d2_RepairFigNo_ofs = 9    '「被疑部品番号」列
Public Const d2_RepairPlace_ofs = 10   '「被疑部品実装番号」列
Public Const d2_DetectCond_ofs = 11    '「detection condition」列 ※Reserve
Public Const d2_data_max_ofs = 11  '最終列

Public Sub 未実装()
    Call MsgBox("未実装", vbInformation)
End Sub

Public Sub 終了_Click()
    Application.StatusBar = False
    
    On Error Resume Next
    Call close_RefBooks
    On Error GoTo 0
    
    If Workbooks.Count <= 1 Then
        Application.Quit
    Else
        ThisWorkbook.Close
    End If
End Sub

Public Sub 一括実行_Click()
    If toolPara_get("UnitFigNo", 省略可:=False) = "" Then
        Exit Sub
    End If
    
    If MsgBox("一括実行を開始します。", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    
    Dim strInputPath As String
    strInputPath = TECHINFO格納フォルダ名取得()
    If strInputPath = "" Then
        Exit Sub
    End If
    
    Dim intResult    As Variant '被疑部品検出件数
    intResult = 一括実行_Execute(strInputPath)
    If intResult > 0 Then
        MsgBox "一括実行終了 (" & intResult & "件検出)"
    End If
End Sub

Public Function 一括実行_Execute(ByVal strInputPath As String) As Variant
    Const job_name = "一括実行"
    Dim varResult   As Variant '実行結果
    Dim intFound    As Variant '被疑部品検出件数
    
    Call job_begin(job_name)
    
    varResult = ""
    
    'UNIT図版取得
    If toolPara_get("UnitFigNo", 省略可:=False) = "" Then
    ElseIf TECHINFO展開_Execute(strInputPath) = False Then
    ElseIf ログ解析_Execute() < 1 Then
    Else
        intFound = 検索_Execute()
        If intFound > 0 Then
            varResult = True
        End If
    End If
    
    Call job_end(job_name, varResult)
    
    一括実行_Execute = intFound
End Function

'--------------------------------------------------------------
' データ領域クリア
'--------------------------------------------------------------
Private Sub range_clear(rng As Range)
    rng.Clear
    rng.Locked = False
End Sub

'ログ解析結果エリアのクリア
Public Sub ログ解析結果クリア_Click()
    Application.StatusBar = False
    If MsgBox("ログ解析結果をクリアします。", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    Application.EnableEvents = False
    Call area_clear(1)
    Application.EnableEvents = True
End Sub

'データ検索結果エリアのクリア
Public Sub 検索結果クリア_Click()
    Application.StatusBar = False
    If MsgBox("データ検索結果をクリアします。", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    Application.EnableEvents = False
    Call area_clear(2)
    Application.EnableEvents = True
End Sub

'日時指定エリアのクリア
Public Sub 日時指定クリア_Click()
    Application.StatusBar = False
    If MsgBox("日時指定をクリアします。", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    Application.EnableEvents = False
    Call area_clear(3)
    Application.EnableEvents = True
End Sub

'一括クリア
Public Sub 一括クリア_Click()
    Application.StatusBar = False
    If MsgBox("全ての実行結果をクリアします。", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    Application.EnableEvents = False
    Call area_clear(99)
    Call area_clear(3)
    Call area_clear(4)
    Application.EnableEvents = True
End Sub

Public Sub area_clear(area_no As Long)
    Dim ws  As Worksheet
    Dim r1  As Range
    Dim r2  As Range
    Dim max_row As Long
    Dim max_col As Long
    
    Set ws = GetWorkSheetByName(control_shname, ThisWorkbook)
    max_row = ws.Rows.Count
    
    Call MyProtectReset(ws)
    
    Select Case area_no
    Case 0, 99  'ログテキスト領域
        Dim wsText As Worksheet
        Set wsText = GetWorkSheetByName(sPasteLog_shname, ThisWorkbook)
        If Not wsText Is Nothing Then
            wsText.Cells.Clear
        End If
        Set wsText = Nothing
    End Select
    
    Select Case area_no
    Case 1, 99 'ログ解析結果エリア
        max_col = ws.Columns(d2_data_base_col).Offset(0, -1).Column
        Set r1 = ws.Cells(data_base_line + 1, 1)
        Set r2 = ws.Cells(max_row, max_col)
        Call range_clear(ws.Range(r1, r2))
    End Select
    
    Select Case area_no
    Case 2, 99  'データ検索結果エリア
        If area_no = 2 Then
            Set r1 = ws.Cells(data_base_line + 1, d1_data_base_col)
            Set r2 = ws.Cells(max_row, r1.Column + d1_data_max_ofs)
            ws.Range(r1, r2).Interior.ColorIndex = xlNone
        End If
        max_col = ws.Columns(d2_data_base_col).Column + d2_data_max_ofs + 1
        Set r1 = ws.Cells(data_base_line + 1, d2_data_base_col)
        Set r2 = ws.Cells(max_row, max_col)
        Call range_clear(ws.Range(r1, r2))
    End Select
    
    Select Case area_no
    Case 0, 1, 2, 99
        
    Case 3  '日時指定
        Call toolPara_set("TargetDate") '日付
        Call toolPara_set("TargetDateRange") '日数
        
        Call toolPara_set("TargetTime") '時刻
        Call toolPara_set("TargetTimeRange", "３秒前まで")
        
    Case 4  'UNIT図番
        Call toolPara_set("UnitFigNo")
        Call toolPara_set("BladeName")   'Blade名
        
    Case Else
        MsgBox "BUG!", vbCritical
    End Select
    
    Call MyScreenUpdate
    
    Set r1 = Nothing
    Set r2 = Nothing
    Set ws = Nothing
End Sub

'--------------------------------------------------------------
' エラーデータのマーク
'--------------------------------------------------------------
Public Sub set_error_mark(r As Range)
    r.Borders.LineStyle = xlContinuous
    r.Interior.Color = vbRed
End Sub

'--------------------------------------------------------------
' 検出結果を設定
'--------------------------------------------------------------
Public Sub set_result(r As Range, v As Variant _
        , Optional strForm As String = "@", Optional bWrapText As Boolean = True)
    r.Borders.LineStyle = xlContinuous
    r.HorizontalAlignment = xlLeft
    r.VerticalAlignment = xlTop
    r.WrapText = bWrapText
    r.NumberFormatLocal = strForm
    
    r.Value = v
End Sub

'--------------------------------------------------------------
' ジョブ開始処理
'--------------------------------------------------------------
Public Sub job_begin(job_name As String)
    Application.StatusBar = job_name & " 開始"
    Application.EnableEvents = False
    Application.ScreenUpdating = False
    verbose_flag = False
'    verbose_flag = True '試験用
End Sub

'--------------------------------------------------------------
' ジョブ終了処理
'--------------------------------------------------------------
Public Sub job_end(job_name As String, ret_code As Variant)
    Dim strMsg As String
    If VarType(ret_code) <> vbBoolean Then
        strMsg = ret_code
    ElseIf ret_code Then
        strMsg = "完了"
    Else
        strMsg = "ＮＧ"
    End If
    
    Application.ScreenUpdating = True
    Application.EnableEvents = True
    If strMsg <> "" Then
        Application.StatusBar = job_name & " " & strMsg
    End If
    On Error Resume Next
    Call MySheetActivate(ThisWorkbook.Worksheets(control_shname))
    On Error GoTo 0
End Sub

'--------------------------------------------------------------
' シート変更イベントの処理
'--------------------------------------------------------------
Public Sub control_ws_change(Target As Range)
    Dim r1 As Range
    Set r1 = toolParaArea_get("UnitFigNo")
    If r1 Is Nothing Then Exit Sub
    
    If Target.Address(External:=True) = r1.Address(External:=True) Then
        Dim varSave As Variant: varSave = Application.EnableEvents
        Dim varBladeName As Variant
        varBladeName = BladeName_pickup()
        Application.EnableEvents = False
        If IsEmpty(varBladeName) Then
            Call toolPara_set("BladeName")
        Else
            Call toolPara_set("BladeName", varBladeName)
        End If
        Application.EnableEvents = varSave
    End If
    Set r1 = Nothing
End Sub

'--------------------------------------------------------------
'
'   ログ解析処理
'
'--------------------------------------------------------------
Public Sub ログ解析_Click()
    If MsgBox("ログ解析を開始します。", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    
    Dim intResult As Variant 'エラーログ検出件数
    intResult = ログ解析_Execute()
    If intResult > 0 Then
        MsgBox "ログ解析 完了(" & intResult & "件検出)"
    End If
End Sub

Public Sub ログ解析結果絞込み_Click()
    If MsgBox("ログ解析結果から指定日時範囲外のデータを削除します。", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    
    Dim intResult As Variant 'エラーログ検出件数
    intResult = ログ解析結果絞込み_Execute()
    If intResult > 0 Then
        MsgBox "完了(" & intResult & "件抽出)"
    End If
End Sub

'--------------------------------------------------------------
'
'   データ検索実行
'
'--------------------------------------------------------------
Public Sub 全検索_Click()
    If MsgBox("全検索を開始します。", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    
    Dim intResult As Variant '被疑部品検出件数
    intResult = 検索_Execute()
    If intResult > 0 Then
        MsgBox "全検索 完了(" & intResult & "件検出)"
    End If
End Sub

Public Sub 選択検索_Click()
    If MsgBox("選択検索を開始します。", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    
    Dim intResult As Variant '被疑部品検出件数
    intResult = 検索_Execute(Selection)
    If intResult > 0 Then
        MsgBox "選択検索 完了(" & intResult & "件検出)"
    End If
End Sub

Public Function detectMode_get() As Long
    Select Case toolPara_get("DetectFilter")
    Case "あり"
        detectMode_get = 1
    Case Else
        detectMode_get = 0
    End Select
End Function

'Blade名取り出し
Public Function BladeName_pickup( _
        Optional ByVal 省略可 As Boolean = True) As Variant
        
    BladeName_pickup = Empty
    
    'UNIT図番取り出し
    Dim unit_figno As String
    unit_figno = toolPara_get("UnitFigNo", 省略可:=省略可)
    If unit_figno = "" Then Exit Function
    
    Dim varRefTable As Variant
    varRefTable = RepairPartTableGet(unit_figno, OnlyBladeName:=True)
    If IsEmpty(varRefTable) Then Exit Function
    
    BladeName_pickup = varRefTable(8)
End Function

'--------------------------------------------------------------
' 参照ファイルの定義を返す
'--------------------------------------------------------------
Public Function RepairPartTableGet( _
        ByVal unit_figno As String, _
        Optional OnlyBladeName As Boolean = False) As Variant
    
    Dim varRefTable(8) As Variant
    Dim tabList As ListObject
    Dim tabRow As ListRow
    Dim tabCol As ListColumn
    Dim intFound As Long
    
    Dim wbRef As Workbook
    Dim wsResult  As Worksheet
    Dim c1 As Range
    Dim c2 As Range
    Dim strKey As String
    Dim strName As String
    
    RepairPartTableGet = Empty
    
    Set tabList = toolParaUnitNoTable_get()
    
    intFound = 0
    Set tabCol = tabList.ListColumns("UNIT図番")
    For Each c1 In tabCol.DataBodyRange.Cells
        If Trim(c1.Value) = unit_figno Then
            intFound = c1.Row - tabCol.DataBodyRange.Row + 1
            Exit For
        End If
    Next c1
    
    If intFound = 0 Then
        Set c1 = tabCol.Range.Cells(1)
        Call MyRangeSelect(c1)
        Call MsgBox("設定エラー at " & c1.Address & vbCrLf & vbCrLf _
                   & "'" & unit_figno & "' が未登録です。", vbCritical)
        GoTo Exit_Proc
    End If
    
    Set tabRow = tabList.ListRows(intFound)
    
    Dim strBladeName As String
    Set tabCol = tabList.ListColumns("Blade名")
    Set c1 = tabRow.Range.Columns(tabCol.Index)
    strBladeName = MySpaceCharsReplace(c1.Value, "")
    If strBladeName = "" Then
        Call MyRangeSelect(c1)
        Call MsgBox("設定エラー at " & c1.Address & vbCrLf & vbCrLf _
                  & "'" & unit_figno & "' の" & tabCol.Name & "が未設定です。", vbCritical)
        GoTo Exit_Proc
    End If
    varRefTable(8) = strBladeName
    
    If OnlyBladeName Then
        GoTo Normal_End
    End If
    
    Set tabCol = tabList.ListColumns("ファイル名")
    Set c1 = tabRow.Range.Columns(tabCol.Index)
    Call MyRangeSelect(c1)
    
    strName = Trim(c1.Value)
    If IsSpaceString(strName) Then
        Call MsgBox("設定エラー at " & c1.Address & vbCrLf & vbCrLf _
                  & "'" & unit_figno & "' の" & tabCol.Name & "が未設定です。", vbCritical)
        GoTo Exit_Proc
    End If
    
    Set wbRef = MyRefBookOpen(strName)
    If wbRef Is Nothing Then
        GoTo Exit_Proc
    End If
    
    Set tabCol = tabList.ListColumns("FLT要因シート名")
    Set c1 = tabRow.Range.Columns(tabCol.Index)
    strName = Trim(c1.Value)
    If IsSpaceString(strName) Then
        Call MyRangeSelect(c1)
        Call MsgBox("設定エラー at " & c1.Address & vbCrLf & vbCrLf _
                  & "'" & unit_figno & "' の" & tabCol.Name & "が未設定です。", vbCritical)
        GoTo Exit_Proc
    End If
    
    Set tabCol = tabList.ListColumns("検索開始行")
    Set c1 = tabRow.Range.Columns(tabCol.Index)
    On Error Resume Next
    intFound = c1.Value
    If Err.Number <> 0 Then
        intFound = 0
    End If
    On Error GoTo 0
    Select Case intFound
    Case Is < 1, Is > 1000
        Call MsgBox("設定エラー at " & c1.Address & vbCrLf & vbCrLf _
                  & "'" & unit_figno & "' の" & tabCol.Name & "の値が不正です。", vbCritical)
        GoTo Exit_Proc
    End Select
    
    Set wsResult = GetWorkSheetByName(strName, wbRef)
    If wsResult Is Nothing Then
        GoTo Exit_Proc
    End If
    
    Dim i As Long
    Dim strCol As String
    For i = 1 To 6
        Select Case i
        Case 1: strKey = "Addr列"
        Case 2: strKey = "Bit列"
        Case 3: strKey = "BitName列"
        Case 4: strKey = "部品名列"
        Case 5: strKey = "部品番号列"
        Case 6: strKey = "実装位置列"
         Case Else
            Call MsgBox("Bug!!", vbCritical)
        End Select
        
        Set tabCol = tabList.ListColumns(strKey)
        Set c1 = tabRow.Range.Columns(tabCol.Index)
        strCol = Trim(c1.Value)
        
        On Error Resume Next
        Set c2 = wsResult.Range(strCol & intFound)
        If Err.Number <> 0 Then
            Set c2 = Nothing
        End If
        On Error GoTo 0
        If c2 Is Nothing Then
            Call MyRangeSelect(c1)
            Call MsgBox("設定エラー at " & c1.Address & vbCrLf & vbCrLf _
                      & strKey & "の値が不正です。", vbCritical)
            GoTo Exit_Proc
        End If
        
        If i = 1 Then
            Set varRefTable(i) = c2
        Else
            varRefTable(i) = c2.Column
        End If
    Next i
    
    varRefTable(0) = get_lastDataCell(wsResult).Row

    ' Blade名がT200,T700の場合、_flt.txt一覧シート名は無視する
    If (strBladeName = "T200") Or (strBladeName = "T700") Then
         Set varRefTable(7) = Nothing
         GoTo Normal_End
    End If
    
    Set tabCol = tabList.ListColumns("_flt.txt一覧シート名")
    Set c1 = tabRow.Range.Columns(tabCol.Index)
    strName = Trim(c1.Value)
    If IsSpaceString(strName) Then
        Call MyRangeSelect(c1)
        Call MsgBox("設定エラー at " & c1.Address & vbCrLf & vbCrLf _
                  & "'" & unit_figno & "' の" & tabCol.Name & "が未設定です。", vbCritical)
        GoTo Exit_Proc
    End If
    Set wsResult = GetWorkSheetByName(strName, wbRef)
    If wsResult Is Nothing Then
        GoTo Exit_Proc
    End If
    
    Set varRefTable(7) = wsResult.Range("B2")
    
Normal_End:
    RepairPartTableGet = varRefTable
    
Exit_Proc:
    Erase varRefTable
    Set tabRow = Nothing
    Set tabCol = Nothing
    Set tabList = Nothing
End Function

'--------------------------------------------------------------
'
' 参照ファイル一括クローズ
'
'--------------------------------------------------------------
Public Sub 参照ファイルクローズ_Click()
    Call close_RefBooks
End Sub

Private Sub close_RefBooks()
    Dim wb  As Workbook
    
    On Error Resume Next
    For Each wb In Application.Workbooks
        If wb.ReadOnly Then
            wb.Close SaveChanges:=False
        End If
    Next wb
    Err.Clear
End Sub

'--------------------------------------------------------------
'   ツール設定
'--------------------------------------------------------------
Private Function toolParaArea_get(sKey As String) As Range
    
    Set toolParaArea_get = Nothing
    
    Dim strSheetName As String
    If sKey Like "Swerr*" Then
        strSheetName = deftab_shname
    ElseIf sKey Like "Auto*" Then
        strSheetName = deftab_shname
    Else
        strSheetName = control_shname
    End If
    
    Dim ws  As Worksheet
    Set ws = GetWorkSheetByName(strSheetName, ThisWorkbook)
    If ws Is Nothing Then Exit Function
    
    Dim rngResult As Range
    Select Case sKey
    Case "SwerrTool"    'SWERRログ解凍ツールパス
        Set rngResult = 名前の参照範囲取得(ws, strDefName_ExtractTool)
        
    Case "SwerrOut"     'SWERRログ解凍先フォルダ
        Set rngResult = 名前の参照範囲取得(ws, strDefName_ExtractOutput)
        
    Case "SwerrIn"      'SWERRログ解凍元ファイル格納フォルダ
        Set rngResult = 名前の参照範囲取得(ws, strDefName_ExtractInput)
        
    Case "AutoFLTLOG"
        Set rngResult = 名前の参照範囲取得(ws, strDefName_FLTLOG)
        
    Case "AutoMacroFlag"
        Set rngResult = 名前の参照範囲取得(ws, strDefName_MacroFlag)
        
    Case "AutoSaveFile"
        Set rngResult = 名前の参照範囲取得(ws, strDefName_SaveFile)
        
    Case "TargetDate", "TargetDateRange", "TargetDateUnit"
        '抽出範囲_日付
        Set rngResult = 名前の参照範囲取得(ws, para1_base)
        If rngResult Is Nothing Then Exit Function
        
        Select Case sKey
        Case "TargetDate":      Set rngResult = rngResult.Cells(1)
        Case "TargetDateRange": Set rngResult = rngResult.Cells(2)
        Case "TargetDateUnit":  Set rngResult = rngResult.Cells(2).Offset(0, 1)
        Case Else
            Call MsgBox("Bug!!", vbCritical)
            Stop
            Exit Function
        End Select
        
    Case "TargetTime", "TargetTimeRange"
        '抽出範囲_時刻
        Set rngResult = 名前の参照範囲取得(ws, para2_base)
        If sKey = "TargetTime" Then
            Set rngResult = rngResult.Cells(1)
        Else
            Set rngResult = rngResult.Cells(2)
        End If
        
    Case "UnitFigNo"  'UNIT図版
        Set rngResult = 名前の参照範囲取得(ws, para3_base)
        
    Case "BladeName"   'Blade名
        Set rngResult = 名前の参照範囲取得(ws, para5_base)
        
    Case "DetectFilter" '出力フィルタ
        Set rngResult = 名前の参照範囲取得(ws, para4_base)
        
    Case Else
        Call MsgBox("Bug!!", vbCritical)
        Stop
        Exit Function
    End Select
    
    Set toolParaArea_get = rngResult
End Function

Private Sub toolPara_set( _
        strKey As String, _
        Optional varValue As Variant)
        
    Dim rngPara As Range
    Set rngPara = toolParaArea_get(strKey)
    If rngPara Is Nothing Then Exit Sub
    
    Call MyProtectReset(rngPara.Worksheet)
    
    If IsMissing(varValue) Then
        rngPara.ClearContents
    Else
        If VarType(varValue) = vbString Then
            rngPara.NumberFormatLocal = "@"
        End If
        rngPara.Value = varValue
    End If
End Sub

Public Function toolPara_get( _
        sKey As String, _
        Optional ByVal 省略可 As Boolean = True) As String
            
    toolPara_get = ""
    
    Dim rngPara As Range
    Set rngPara = toolParaArea_get(sKey)
    If rngPara Is Nothing Then Exit Function
    
    Dim strValue As String
    Select Case sKey
    Case "TargetDate", "TargetTime"
        strValue = Trim(rngPara.Text) '注意) TargetTimeは、.Textで取り出さないと時刻範囲の判定ができない
    Case Else
        strValue = Trim(rngPara.Value)
    End Select
    If IsSpaceString(strValue) Then
        strValue = ""
    End If
    
    If Not 省略可 And (strValue = "") Then
        Call MyRangeSelect(rngPara)
        Call MsgBox(rngPara.Address & " が未設定です。", vbCritical)
        Exit Function
    End If
    
    toolPara_get = strValue
End Function

'UNIT図番テーブル取得
Private Function toolParaUnitNoTable_get() As ListObject
    Set toolParaUnitNoTable_get = Nothing
    
    Dim ws  As Worksheet
    Set ws = GetWorkSheetByName(deftab_shname, ThisWorkbook)
    If ws Is Nothing Then Exit Function
    
    Set toolParaUnitNoTable_get = GetTableByName(strTabName_UnitNoList, ws)
        
End Function

Public Sub TECHINFO解凍ツール設定_Click()
    
    'SWERRログ解凍ツールのフルパスを指定する
    Dim fd As FileDialog
    Dim strInit   As String
    Dim vrtSelectedItem As Variant
    
    strInit = toolPara_get("SwerrTool")
    If strInit <> "" Then
        strInit = MyFullPathGet(strInit)
        If Not IsExistPath(strInit) Then
            strInit = ""
        End If
    End If
    
    If strInit = "" Then
        strInit = ThisWorkbook.Path
    End If
    
    Set fd = Application.FileDialog(msoFileDialogFilePicker)
    With fd
        .Title = "TECHINFO解凍ツールを指定"
        
        .Filters.Clear
        .Filters.Add "実行ファイル", "*.bat"
        .Filters.Add "すべてのファイル", "*.*"
        .FilterIndex = 1
        .InitialFileName = strInit
        .AllowMultiSelect = False
        If .Show = -1 Then
            For Each vrtSelectedItem In .SelectedItems
                Call toolPara_set("SwerrTool", vrtSelectedItem)
            Next vrtSelectedItem
        End If
    End With
    Set fd = Nothing

End Sub

Public Sub TECHINFO格納フォルダ設定_Click()
    
    Dim strInit   As String
    Dim strResult As String
    
    strInit = toolPara_get("SwerrIn")
    strResult = フォルダ選択("TECHINFO格納フォルダ", strInit)
    If strResult <> "" Then
        Call toolPara_set("SwerrIn", strResult)
    End If

End Sub

Public Sub TECHINFO解凍先フォルダ設定_Click()
    
    Dim strInit   As String
    Dim strResult As String
        
    strInit = toolPara_get("SwerrOut")
    strResult = フォルダ選択("TECHINFO解凍先フォルダ", strInit)
    If strResult <> "" Then
        Call toolPara_set("SwerrOut", strResult)
    End If

End Sub

Private Function フォルダ選択( _
        ByVal strTitle As String, _
        Optional ByVal strInitPath As String = "") As String
    
    フォルダ選択 = ""
    
    Dim fd As FileDialog
    Dim strFolderPath As String
    
    If IsSpaceString(strInitPath) Then
        strFolderPath = ""
    Else
        strFolderPath = 有効なフォルダ名を取得(MyFullPathGet(strInitPath))
    End If
    
    If strFolderPath = "" Then
        strFolderPath = ThisWorkbook.Path
    End If
    
    Set fd = Application.FileDialog(msoFileDialogFolderPicker)
    With fd
        .Title = strTitle & "を指定"
        .InitialFileName = strFolderPath & "\"
        If .Show = -1 Then
            フォルダ選択 = .SelectedItems(1)
        End If
    End With
    Set fd = Nothing

End Function

Private _
Function 有効なフォルダ名を取得(strFullPath As String) As String
    Dim strFolderPath As String
    Dim strSave As String
        
    strFolderPath = MyFolderNameGet(strFullPath)
    Do Until IsExistPath(strFolderPath)
        strSave = strFolderPath
        strFolderPath = MyFolderNameGet(strFolderPath)
        If strFolderPath = strSave Then
            strFolderPath = ""
            Exit Do
        End If
    Loop
    
    有効なフォルダ名を取得 = strFolderPath
End Function

'--------------------------------------------------------------
'
' TECHINFOログ解凍
'
'--------------------------------------------------------------
Public Sub TECHINFO展開_Click()
    Dim strInputPath As String
    
    Call toolPara_set("AutoMacroFlag", "")
    
    strInputPath = TECHINFO格納フォルダ名取得()
    If strInputPath = "" Then
        Exit Sub
    End If
        
    If TECHINFO展開_Execute(strInputPath) Then
        Call MsgBox("TECHINFO展開 完了")
    End If
    
    Call toolPara_set("AutoMacroFlag", "1")
End Sub

Public Function TECHINFO格納フォルダ名取得() As String
    TECHINFO格納フォルダ名取得 = ""
    
    Dim logfile As String
    Dim strInputPath As String
    
    strInputPath = ""
    logfile = toolPara_get("SwerrIn")
    If logfile <> "" Then
        logfile = MyFullPathGet(logfile)
        If IsExistPath(logfile) Then
            Select Case MsgBox("'" & logfile & "'" & vbCrLf & vbCrLf _
                      & "から抽出します。", vbYesNoCancel)
            Case vbYes
                strInputPath = logfile
            Case vbCancel
                Exit Function
            End Select
        End If
    End If
    
    If strInputPath = "" Then
        strInputPath = フォルダ選択("TECHINFO格納フォルダ", logfile)
        If strInputPath = "" Then
            Exit Function
        End If
    End If
    
    TECHINFO格納フォルダ名取得 = strInputPath
End Function

Sub 実行結果保存_Click()
    If MsgBox("実行結果を保存します。", vbOKCancel) = vbCancel Then
        Exit Sub
    End If
    
    If 実行結果保存_Execute(DoClose:=True) Then
        Call MsgBox("実行結果保存 完了")
    End If
End Sub

Sub 名前を付けて保存()
'
' 名前を付けて保存 Macro
'
    Call toolPara_set("AutoMacroFlag", "")

    Worksheets(control_shname).Activate
    
    Application.DisplayAlerts = False '問い合せダイアログの表示をOFFにします
    
    ActiveWorkbook.SaveAs Filename:= _
        toolPara_get("AutoSaveFile") _
        , FileFormat:=xlOpenXMLWorkbookMacroEnabled, CreateBackup:=False '名前を付けて保存
        
    Application.DisplayAlerts = True '問い合せダイアログの表示をONにします
    
    Call toolPara_set("AutoMacroFlag", "1")
   
End Sub

Sub EXCEL終了()

    Call toolPara_set("AutoMacroFlag", "")
    
    Application.DisplayAlerts = False
    
    'Worksheets("操作").Activate
          
    Application.Quit                     'Excelを終了します

End Sub

